extends Node

var sfx_stream_player = AudioStreamPlayer.new()
var bg_stream_player = AudioStreamPlayer.new()

enum SFX{
		# FIGHT
		WALK,
		SWORD_ATTACK,
		BOW_ATTACK,
		TAKE_DAMAGE,
		PASS,
		INTERCEPTION,
		STEAL_FAILED,
		BONUS,
		DEATH,
		CURSOR,
		DRUMS,
		# MENUS
		OPTION_SELECT,
		PLAYER_SELECT,
		READY,
		CANCEL,
		START_GAME
		# MISC
		LEO
	}

enum BG{
	TITLE,
	BATTLE,
	INTRO
}

var sfx = {
	# FIGHT
	SFX.TAKE_DAMAGE: preload("res://assets/audio/sfx/take_damage02.wav"),
	SFX.SWORD_ATTACK: preload("res://assets/audio/sfx/simple_hit.wav"),
	SFX.BOW_ATTACK: preload("res://assets/audio/sfx/shoot.wav"),
	SFX.PASS: preload("res://assets/audio/sfx/swosh-19.wav"),
	SFX.WALK: preload("res://assets/audio/sfx/step_grass.wav"),
	SFX.INTERCEPTION: preload("res://assets/audio/sfx/coup_critique.wav"),
	SFX.STEAL_FAILED: preload("res://assets/audio/sfx/ole.wav"),
	SFX.CURSOR: preload("res://assets/audio/sfx/Click02.wav"),
	SFX.BONUS: preload("res://assets/audio/sfx/bonus.ogg"),
	SFX.DEATH: preload("res://assets/audio/sfx/death.wav"),
	SFX.DRUMS: preload("res://assets/audio/sfx/drum_roll.wav"),
	# Menu
	SFX.OPTION_SELECT: preload("res://assets/audio/sfx/Menu_Select.wav"),
	SFX.PLAYER_SELECT: preload("res://assets/audio/sfx/Click.wav"),
	SFX.READY: preload("res://assets/audio/sfx/MENU B_Select.wav"),
	SFX.CANCEL: preload("res://assets/audio/sfx/MENU B_Back.wav")
	}

var bg = {
	BG.TITLE: preload("res://assets/audio/music/title_theme.ogg"),
	BG.BATTLE: preload("res://assets/audio/music/battle_theme.ogg"),
	BG.INTRO: preload("res://assets/audio/intro/music.ogg")
}


func _ready():
	# Get root node
	var root_id = get_tree().get_root().get_child_count() - 1
	var root_node = get_tree().get_root().get_child(root_id)
	
	# Init sfx stream player
	sfx_stream_player.bus = "SFX"
	root_node.add_child(sfx_stream_player)
	
	# Init sfx stream player
	bg_stream_player.bus = "BackgroundMusic"
	bg_stream_player.pause_mode = PAUSE_MODE_STOP
	root_node.add_child(bg_stream_player)
	
	

func play_sfx(var sfx_id, var stream_player = null, var random_pitch = true) :
	if sfx.has(sfx_id) :
		
		var audio = sfx[sfx_id]
		
		var audio_stream 
		if random_pitch :
			audio_stream = AudioStreamRandomPitch.new()
			audio_stream.set_audio_stream(audio)
		else :
			audio_stream = audio
		
		
		var sp = sfx_stream_player
		if stream_player : 
			sp = stream_player
		
		sp.stream = audio_stream
		sp.play()

func play_background_music(var bg_id) :
	if bg.has(bg_id) :
		bg_stream_player.stream = bg[bg_id]
		bg_stream_player.play()

func set_BG_db(var db):
	bg_stream_player.volume_db = db
	
func get_BG_db():
	return bg_stream_player.volume_db

# TODO Fade to music

func stop_background_music() :
	bg_stream_player.stop()

func pause_background_music() :
	bg_stream_player.playing = false

func unpause_background_music() :
	bg_stream_player.playing = true

func stop_sfx(var stream_player = null) :
	if stream_player :
		stream_player.stop()
	else :
		sfx_stream_player.stop()



#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
