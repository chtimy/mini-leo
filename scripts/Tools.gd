extends Node

class CustomSort:
	static func sort_turns(a, b):
		if a.turn < b.turn:
			return true
		return false

func ready():
	randomize()

func manhattan_dist(var p1, var p2):
	return abs(p1.x - p2.x) + abs(p1.y - p2.y)
	
func circle_area_func(var position, var target_position, var args):
	if Tools.manhattan_dist(position, target_position) <= args[0]:
		if position == target_position :
			if args.size() > 1 && args[1] == true :
				return true
		else:
			return true
	return false
	
func randomize_array(var array):
	var n = array.size()
	for i in range(n-1):
		var k = (randi()%(n-i))
		var tmp = array[k]
		array[k] = array[i]
		array[i] = tmp
	return array
	
func throw_dice(var maxNumber):
	return randi() % maxNumber + 1
	
func draw_arrow(var geometry, var points, var size_cell): 
	var other_side = []
	var size_cell_quarter = size_cell / 8
	var size_cell_half = size_cell / 4
	for i in range(points.size()):
		var j = points.size() - i - 1
		var current_point = Tools.get_cell_center_position(points[j], size_cell)
		#end of path + arrow
		if j == 0:
			var previous_point = Tools.get_cell_center_position(points[j + 1], size_cell)
			var direction_normalized = (current_point - previous_point).normalized()
			var ortho = Vector2(-direction_normalized.y, direction_normalized.x)
			var pt = current_point + ortho * size_cell_quarter - direction_normalized * size_cell_half * 1.5
			var pt3 = current_point - ortho * size_cell_quarter - direction_normalized * size_cell_half * 1.5
			geometry.push_back(pt)
			other_side.push_back(pt3)
#			geometry.push_back(current_point + ortho * size_cell_half)
#			geometry.push_back(current_point + direction_normalized * size_cell_quarter)
#			geometry.push_back(current_point - ortho * size_cell_half)
			var pt4 = current_point - direction_normalized * size_cell_half * 1.5
			geometry.push_back(pt4 + ortho * size_cell_half)
			geometry.push_back(pt4 + direction_normalized * size_cell_half)
			geometry.push_back(pt4 - ortho * size_cell_half)
		#beginning of path
		elif j == points.size() - 1:
			var next_point = Tools.get_cell_center_position(points[j - 1], size_cell)
	#		current_point.y += 0.5
			var direction_normalized = (next_point - current_point).normalized()
			var ortho = Vector2(-direction_normalized.y, direction_normalized.x)
			var vec = (direction_normalized + ortho).normalized()
			var pt = current_point + ortho * size_cell_quarter# - direction_normalized * size_cell_quarter#
			var pt2 = current_point - ortho * size_cell_quarter
			geometry.push_back(pt)
			other_side.push_back(pt2)
		#middle of path
		else:
			var next_point = Tools.get_cell_center_position(points[j - 1], size_cell)
	#		current_point.y += 0.5
			var direction_normalized = (next_point - current_point).normalized()
			var ortho = Vector2(-direction_normalized.y, direction_normalized.x)
			var previous_point = Tools.get_cell_center_position(points[j + 1], size_cell)
			var direction_normalized2 = (next_point - previous_point).normalized()
			var pt
			var pt2
			if direction_normalized2 != direction_normalized:
				direction_normalized2 = Vector2(-direction_normalized2.y, direction_normalized2.x)
				pt = current_point + direction_normalized2 * size_cell_quarter * sqrt(2)# - direction_normalized * size_cell_quarter#
				pt2 = current_point - direction_normalized2 * size_cell_quarter * sqrt(2)
			else:
				pt = current_point + ortho * size_cell_quarter# - direction_normalized * size_cell_quarter#
				pt2 = current_point - ortho * size_cell_quarter
			geometry.push_back(pt)
			other_side.push_back(pt2)
	var size = other_side.size()
	for i in range(other_side.size()):
		geometry.push_back(other_side[size - i - 1])
	return geometry
	
func compare_nodes(var node1, var node2):
	if node1.cost_f < node2.cost_f:
		return true
	return false
	
func euclidian_dist(var start, var target):
	return (start.x - target.x) * (start.x - target.x) + (start.y - target.y) * (start.y - target.y)
	
func shortest_path(var start, var target, var matrix, var black_list_groups = [], var white_list_groups = []):
	var list_neighbor_ofsets = [Vector2(1,0), Vector2(0,1), Vector2(-1,0), Vector2(0,-1)]
	var closed_list = {}
	var open_list = []
	var dist = euclidian_dist(start, target)
	var current_node = {"position" : start, "cost_g" : 0, "cost_h" : dist, "cost_f" : dist, "parent" : start}
	open_list.push_back(current_node)
	while (current_node.position.x != target.x || current_node.position.y != target.y) && !open_list.empty():
		# take the best node
		current_node = open_list.pop_front()
		# add node in the closed list
		closed_list[current_node.position] = current_node
		# search neighbor and add to the opened list
		for offset in list_neighbor_ofsets: 
			var neighbour_position = current_node.position + offset
			if neighbour_position.x < 0 || neighbour_position.x >= matrix.size_mat.x || neighbour_position.y < 0 || neighbour_position.y >= matrix.size_mat.y:
				continue
			var selectable = matrix.get_obj_by_group(neighbour_position, "Characters")
			var black_blocked = false
			var white_blocked = true
			if selectable:
				for group in black_list_groups:
					if selectable.is_in_group(group):
						black_blocked = true
				for group in white_list_groups:
					if selectable.is_in_group(group):
						white_blocked = false
			if black_blocked || !white_blocked:
				continue
						
			# if the neighbour already is not visited
			if !closed_list.has(neighbour_position):
				# distance from start position to the current node position
				var cost_g = closed_list[current_node.position].cost_g + euclidian_dist(neighbour_position, current_node.position)
				# distance from the current node position to the target position
				var cost_h = euclidian_dist(neighbour_position, target)
				# distance from start position to the current node by the finding path 
				# and then distance to the target by piou piou back
				var cost_f = cost_g + cost_h
				# set the parent to build the final path in the end ()
				# it's a position because it's the used key for the map (don't use pointers, not safe with a map -> need to think like communist, not like arnarchist #privatejoke)
				var parent = current_node.position
				# search inside the array ... O(n) not crazy, but no ordered map -> log(n)
				var find_index = -1
				for i in range(open_list.size()):
					if open_list[i].position == neighbour_position:
						find_index = i
						break
				# if the neighbour is in the open list, need to check if the node is better.
				# if not, replace this node by the neighbour
				# + sort the list regards to the criterion
				if find_index != -1:
					var find = open_list[find_index]
					if cost_f < find.cost_f:
						open_list[find_index] = {"position" : neighbour_position, "cost_g" : cost_g, "cost_h" : cost_h, "cost_f" : cost_f, "parent" : parent}
						open_list.sort_custom(self, "compare_nodes")
				else:
					open_list.push_back({"position" : neighbour_position, "cost_g" : cost_g, "cost_h" : cost_h, "cost_f" : cost_f, "parent" : parent})
					open_list.sort_custom(self, "compare_nodes")
		# check if the current node is the target node, if yes, build the final path (for the revolucion!)
		if current_node.position.x == target.x && current_node.position.y == target.y:
			var final_path = []
			var node = closed_list[target]
			final_path.push_back(node.position)
			while node.position != start:
				node = closed_list[node.parent]
				final_path.push_back(node.position)
			return final_path
	return null
	
	
#func save_value(var key, var value):
#	self.values[key] = value
#func get_value(var key):
#	return self.values[key]
#func load_value(var key):
#	var value = self.values[key]
#	self.values[key] = null
#	return value
#func has_value(var key):
#	return self.values.has(key)
#func clear_values():
#	self.values.clear()
	
func search_selectable_in_tab_by_group(var selectables, var group):
	if !selectables:
		return null
	for selectable in selectables:
		if selectable.is_in_group(group):
			return selectable
	return null
	
func search_selectable_in_tab_by_object(var selectables, var object):
	if !selectables:
		return null 
	for i in range(selectables.size()):
		if selectables[i] == object:
			return i
	return -1
	
func right(var position, var orientation, var i = 1):
	return position + Vector2(orientation.y, -orientation.x) * i
func left(var position, var orientation, var i = 1):
	return position - Vector2(orientation.y, -orientation.x) * i
func behind(var position, var orientation, var i = 1):
	return position - orientation * i
func front(var position, var orientation, var i = 1):
	return position + orientation * i
	
func print_error(var message):
	print("##########################################################")
	print("------ERROR------")
	print(message)
	print("##########################################################")
	
#give the top corner position of the cell
func index_to_position(var index, var offset):
	return index * offset
	
func get_cell_center_position(var index, var offset, var origin = Vector2(0,0)):
	return index_to_position(index, offset) + origin + Vector2(offset, offset)/2

# Distance of the point C from the line (AB)
func dist_to_line(var A, var B, var C):
	var v = (B - A).normalized()
	# (x - Ax) * vy = (y - Ay) * vx
	# yvx - xvy - Ayvx + Axvy = 0
	# dist = ((-vy * Cx)+(vx * Cy) - Ayvx + Axvy) / (sqrt(vx*vx + vy*vy))
	return abs((-v.y * C.x)+(v.x * C.y) -A.y*v.x + A.x*v.y) / (sqrt(v.x*v.x+v.y*v.y))

# Get the orthogonal projection of C on line (AB)
func closest_point_to_line(var A, var B, var C):
	var dist = dist_to_line(A,B,C)
	var dist2 = C.distance_to(A)
	var dist_to_p = sqrt(dist2*dist2-dist*dist)
	var point = A + (B-A).normalized()*dist_to_p
	return Vector2(int(round(point.x)), int(round(point.y)))

func get_direction_cste_from_vec(var vec):
	vec = vec.normalized()
	if vec.dot(Vector2(-1,0)) >= 0.5:
		return Global.LEFT
	if vec.dot(Vector2(1,0)) >= 0.5:
		return Global.RIGHT
	if vec.dot(Vector2(0,1)) >= 0.5:
		return Global.DOWN
	if vec.dot(Vector2(0,-1)) >= 0.5:
		return Global.UP
		
func get_action_from_dir(var name, var dir):
	if dir == Global.RIGHT:
		return name + "_" + "right"
	elif dir == Global.LEFT:
		return name + "_" + "left"
	elif dir == Global.DOWN:
		return name + "_" + "down"
	elif dir == Global.UP:
		return name + "_" + "up"

func linear_function(var value, var a, var b):
	return value * a + b;

func get_central_position() :
	var viewport_rect = get_tree().get_root().get_visible_rect()
	return Vector2(viewport_rect.size.x / 2.0, viewport_rect.size.y / 2.0)