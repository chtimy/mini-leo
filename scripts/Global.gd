extends Node

var COLOR_TEAM_0 = Color(1.0,0.0,0.0,1.0) # Red Team
var COLOR_TEAM_1 = Color(0.0,0.0,1.0,1.0) # Blue Team

var init_bonus_graphics = {
	scale = 1.0,
	tempo = 1.5,
	color = Color(1.0,1.0,0.0,1.0)
}

var camera_parameters = {
	"offset" : Vector2(0,0),
	"zoom" : Vector2(1,1)
}

var CAMERA_SHAKE_AMPLITUDE_BONUS_PASS = 2
var CAMERA_SHAKE_FREQUENCY_BONUS_PASS = 40

enum DIRECTION{LEFT, RIGHT, UP, DOWN}
enum TEAM{TEAM_0, TEAM_1}

var SIZE_TIRETS_PASS = {"long" : 20, "width" : 3, "offset" : 30}

var TIME_SHAKE_CAMERA_ATTACK = 0.4

var PASS_SPEED = 150  # in pixels / second

var TURN_HANDLER_PARAM = {
	"offset_height" : 1.3,
	"speed_height" : 0.6,
	"speed_width" : 0.4,
	"scale" : 1.0,
	"color_frame_border" : Color(0.2,0.2,0.2,1),
	"color_frame_border_first" : Color(1, 0, 0, 1)
}