extends Control

signal option_selected

func _ready():
	set_process_input(true)

func _input(event):
	if event.device == 0 && event.is_action_pressed("ui_accept") :
		MUSE.play_sfx(MUSE.OPTION_SELECT)
		emit_signal("option_selected", "Title")
