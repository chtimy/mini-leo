extends Control

signal option_selected

var options = ["Play", "Credits", "Quit"]
var current_option = 0

var timer = Timer.new()

func _ready():
	# Init timer for option selection with joystick
	self.timer.one_shot = true
	self.timer.wait_time = 0.2
	add_child(self.timer)
	
#	var size = get_tree().get_root().get_viewport().get_size()
#	$BG.set_size(Vector2(0, size.y))
	
	set_process_input(true)
	
	if !MUSE.bg_stream_player.playing :
		MUSE.play_background_music(MUSE.INTRO)

func _input(event):
	if event.device == 0 :
		if event.is_action_pressed("ui_accept") :
			emit_signal("option_selected", options[current_option])
		
		if event.is_action_pressed("ui_up") || event.is_action_pressed("ui_down") : 
			if self.timer.is_stopped() :
				# Hide A Button
				get_node("Menu/"+options[current_option]+"/Button").visible = false
				
				if event.is_action_pressed("ui_up") :
					current_option -= 1
					if current_option < 0 :
						current_option = options.size() - 1
				elif event.is_action_pressed("ui_down") :
					current_option = (current_option + 1) % options.size()
				
				# Show A Button
				get_node("Menu/"+options[current_option]+"/Button").visible = true
				
				# Play sound
				MUSE.play_sfx(MUSE.OPTION_SELECT)
				
				self.timer.start()
