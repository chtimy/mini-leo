extends Control

signal option_selected

var options = ["Rematch", "Title", "Quit"]
var current_option = 0

var timer = Timer.new()

func _ready():
	# Init timer for option selection with joystick
	self.timer.one_shot = true
	self.timer.wait_time = 0.2
	add_child(self.timer)
	
	# Just in case
	get_node("RedTeam").visible = false
	get_node("BlueTeam").visible = false
	
	set_process_input(true)

func init(var teams, var winners) :
	# Get the team
	if winners == Global.TEAM_0 :
		get_node("RedTeam").visible = true
	else :
		get_node("BlueTeam").visible = true
	
	# Set winners
	var team = teams[winners]
	for i in team.size() :
		var scene = load(team[i])
		var winner = scene.instance()
		var winner_frames = winner.get_node("AnimatedSprite").frames
		get_node("Player"+String(i)).frames = winner_frames

func _input(event):
	if event.device == 0 :
		if event.is_action_pressed("ui_accept") :
			MUSE.play_sfx(MUSE.OPTION_SELECT)
			emit_signal("option_selected", options[current_option])
		
		if event.is_action_pressed("ui_up") || event.is_action_pressed("ui_down") : 
			if self.timer.is_stopped() :
				# Hide A Button
				get_node("Menu/"+options[current_option]+"/Button").visible = false
				
				if event.is_action_pressed("ui_up") :
					current_option -= 1
					if current_option < 0 :
						current_option = options.size() - 1
				elif event.is_action_pressed("ui_down") :
					current_option = (current_option + 1) % options.size()
				
				# Show A Button
				get_node("Menu/"+options[current_option]+"/Button").visible = true
				
				# Play sound
				MUSE.play_sfx(MUSE.OPTION_SELECT)
				
				self.timer.start()
