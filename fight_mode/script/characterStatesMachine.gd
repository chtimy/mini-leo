extends Node

# \STATE |        |         |            |                     |             |
#    \   | NORMAL | GET_PASS| AFTER_PASS |  AFTER_INTERCEPTION | AFTER_STEAL |
# ACTION\|        |         |            |                     |             |
# -------------------------------------------------------------|-------------|
# Pass   |  YES   |   YES   |    NO      |         YES         |     NO      |
# Move   |  YES	  |   NO    |    YES     |         NO          |     NO      |
# Grab   |  YES   |   NO    |    YES     |         NO          |     NO      |
# Attack |  YES   |   YES   |    NO      |         YES          |     YES     |
# Cancel |  YES   |   YES   |    YES     |         YES         |     YES     |
# Interc |        |         |            |         NO          |     NO      |
# Steal  |  YES   |   NO    |    NO      |         NO          |     NO      |

# Main action : ATTACK, PASS, STEAL
# Block

# NORMAL : (ATTACK OR PASS OR STEAL) + MOVE + GRAB
# GET_PASS : ATTACK OR PASS
# AFTER_PASS : MOVE + GRAB
# AFTER_INTERCEPTION : PASS

enum TYPE_ACTION {NORMAL_STATUS, GET_PASS_STATUS, AFTER_PASS_STATUS, AFTER_INTERCEPT_STATUS, STEAL_STATUS}
enum ACTIONS{PASS, MOVE, GRAB, ATTACK, STEAL}

var actions = [0, 0, 0, 0, 0]
var status = [0, 0, 0, 0, 0]

func start(status_start):
	status[status_start] = 1
	
func set(status_state):
	reset(false)
	self.status[status_state] = 1

func can_make_main_action():
	if status[NORMAL_STATUS] || status[GET_PASS_STATUS] || status[AFTER_INTERCEPT_STATUS]:
		if !actions[PASS] && !actions[ATTACK] && !actions[STEAL]:
			return true
	elif status[STEAL_STATUS]:
		if can_attack():
			return true
	return false
	
func can_make_second_action():
	if status[NORMAL_STATUS]:
		if !actions[MOVE] || !actions[GRAB] || !actions[STEAL]:
			return true
	elif status[GET_PASS_STATUS] || status[AFTER_INTERCEPT_STATUS]:
		if can_pass():
			return true
	elif status[AFTER_PASS_STATUS]:
		if can_grab():
			return true
	elif status[STEAL_STATUS]:
		if can_steal():
			return true
	return false

func can_pass():
	if status[NORMAL_STATUS] || status[GET_PASS_STATUS] || status[AFTER_INTERCEPT_STATUS]:
		if !actions[PASS]:
			return true
	return false

func can_attack():
	if status[NORMAL_STATUS] || status[GET_PASS_STATUS] || status[STEAL_STATUS] || status[AFTER_INTERCEPT_STATUS]:
		if !actions[ATTACK]:
			return true
	return false

func can_grab():
	if status[NORMAL_STATUS] || status[AFTER_PASS_STATUS]:
		if !actions[GRAB]:
			return true
	return false

func can_move():
	if status[NORMAL_STATUS] || status[AFTER_PASS_STATUS]:
		if !actions[MOVE]:
			return true
	return false

func can_steal():
	if status[NORMAL_STATUS] :
		if !actions[STEAL]:
			return true
	return false

func grab_OK():
	actions[GRAB] = 1

func pass_OK():
	actions[PASS] = 1
	
func move_OK():
	actions[MOVE] = 1

func attack_OK():
	actions[ATTACK] = 1

func steal_OK():
	actions[STEAL] = 1

func check_end_turn():
	if status[NORMAL_STATUS] && !can_make_main_action() && !can_make_second_action():
		return true
	elif status[GET_PASS_STATUS] && !can_attack() && !can_pass():
		return true
	elif status[AFTER_PASS_STATUS] && !can_move() && !can_grab():
		return true
	elif status[AFTER_INTERCEPT_STATUS] && !can_pass():
		return true
	return false
	
func reset(var reinit_actions = true):
	for i in status.size():
		status[i] = 0
	if reinit_actions:
		for i in actions.size():
			actions[i] = 0