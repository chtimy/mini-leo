extends "res://fight_mode/script/weapon.gd"

func _init():
	self.function_range = {"f" : funcref(Tools, "circle_area_func"), "args" : [5]}
	set_name("Bow")
	
func active_ghost():
	$GhostParticles.set_visible(true)
	
func disable_ghost():
	$GhostParticles.set_visible(false)