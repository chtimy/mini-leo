extends "res://fight_mode/script/movable.gd"

onready var states = preload("res://fight_mode/script/characterStatesMachine.gd").new()

export(int) var life = 100

export(int) var offense_skill = 10
export(int) var defense_skill = 10
export(int) var reflex_skill = 10
export(int) var teammate_skill = 10
export(int) var move_skill = 10

var nb_move = 5

var distance_pass = 1000

var bonus_timer = Timer.new()
var attack_bonus = 0
var nb_bonus_pass = 0

var interception_timer = null
var interception_dist_max = 3
var weapon
var displacement_function
var last_direction = Global.DOWN
export(Texture) var turn_sprite setget set_turn_sprite

var team setget set_team # also define the device
var teammate_for_pass
var enemis_to_attack = []

#cste
export (int) var OFFSET_ANGLE_IN_DEGREES_FOR_PASS = 10

signal end_turn_signal
signal end_turn_after_pass_signal
signal player_pass_to_signal
signal player_intercepted_by_signal
signal die_signal
signal change_life_value_signal
signal attack_animation_finished

func _ready():
	# min 1, max 5 
	self.nb_move = 1 + self.move_skill / 5
	
	# 1 or 2
	self.interception_dist_max = 1 + self.nb_move / 5
	
	self.bonus_timer.wait_time = 5
	self.bonus_timer.one_shot = true
	self.bonus_timer.connect("timeout", self, "on_bonus_timer_timeout")
	add_child(bonus_timer)
	
	connect("change_life_value_signal", $LifeProgress, "set_value")

func can_grab(var position_index_to):
	if states.can_grab() && self.position_index == position_index_to && map.is_cell_valid(position_index_to, false, ["Objects"], []):
		return true
	return false

# Grab weapon + Update the animation of the player
func grab_weapon(var grabbed_weapon):
	if self.weapon:
		drop_weapon_on_the_floor()
	self.weapon = grabbed_weapon
	
	#update character animation
	var animation = get_animation_name(self.last_direction, "wait")
	set_animation(animation)
	
	#output message
	var s = self.get_name() + " grabs " + weapon.get_name()
	emit_signal("output_signal", s)
	
# Drop weapon on the map. Add the obj add child to the map + add information in the matrix map
# Update the animation of the player
func drop_weapon():
	var dropped_weapon = self.weapon
	self.weapon = null
	
	#update animation of the character
	var animation = get_animation_name(self.last_direction, "wait")
	set_animation(animation)
	
	#output message
	#var s = self.get_name() + " drops " + dropped_weapon.get_name()
	#emit_signal("output_signal", s)
	
	return dropped_weapon

# Grab weapon on the map. Add the obj add child to the map + add information in the matrix map
# Update the animation of the player
func grab_weapon_on_the_floor():
	MUSE.play_sfx(MUSE.OPTION_SELECT, self.stream_player)
	
	# Disable actions level 1
	states.grab_OK()
	#remove graphics and from matrix
	var grabbed_weapon = map.pop_by_group(self.position_index, "Weapons")
	map.remove_graphics_obj(grabbed_weapon)
	#give the weapon to the character
	grab_weapon(grabbed_weapon)
	
# Drop weapon on the map. Add the obj add child to the map + add information in the matrix map
# Update the animation of the player
func drop_weapon_on_the_floor():
	var dropped_weapon = drop_weapon()
	
	if dropped_weapon :
		#Update graphics map and matrix
		self.map.add_graphics_obj(dropped_weapon, self.position_index)
		self.map.add(dropped_weapon, self.position_index)
	
	return dropped_weapon

# Warning : Fonction utilisant yield
func take_damages(var damages):
	var damage = damages * (1.0 - (float(self.defense_skill) / 40.0))
	
	self.life = max(self.life - int(round(damage)), 0)
	emit_signal("change_life_value_signal", self.life)
	
	# ANIMATION ?
	
	MUSE.play_sfx(MUSE.TAKE_DAMAGE, self.stream_player)
	
	var s = self.get_name() + "  takes damages : " + String(int(round(damage)))
	emit_signal("output_signal", s)
	if self.life == 0:
		var die = die()
		if die:
			yield(die, "completed")
	
func opportunity_attack():
	var value = Tools.throw_dice(100)
	if value < self.opportunity_pourcent:
		var s = self.get_name() + " makes an opportunity attack"
		emit_signal("output_signal", s)
		return true
	return false
	
func can_attack(var position_index_to):
	if states.can_make_main_action() && self.weapon && self.weapon.function_range.f.call_func(self.position_index, position_index_to, [self.weapon.function_range.args[0]]):
		var charac = self.map.get_obj_by_group(position_index_to, "Characters")
		if charac && charac.team != self.team:
			return true
	return false

func attack(var target): 
	# stop bonus timer
	self.bonus_timer.stop()
	
	states.attack_OK()
	self.last_direction = Tools.get_direction_cste_from_vec(target.position_index - self.position_index)
	
	#LOG
	var s = self.get_name() + " attacks " + target.get_name()
	emit_signal("output_signal", s)
	
	#set animation
	var animation
	var sfx
	if self.weapon && self.weapon.is_in_group("Swords"):
		animation = Tools.get_action_from_dir("attack_sword", self.last_direction)
		sfx = MUSE.SWORD_ATTACK
	elif self.weapon && self.weapon.is_in_group("Bows"):
		animation = Tools.get_action_from_dir("attack_bow", self.last_direction)
		sfx = MUSE.BOW_ATTACK
		
	var animationPlayer = AnimationPlayer.new()
	var nb_frames = $AnimatedSprite.get_sprite_frames().get_frame_count(animation)
	$AnimatedSprite.connect("frame_changed", self, "on_Animation_frame_changed", [nb_frames, target])
	set_animation(animation)
	$AnimatedSprite.play()
	MUSE.play_sfx(sfx, self.stream_player)
	yield($AnimatedSprite, "animation_finished")
	$AnimatedSprite.disconnect("frame_changed", self, "on_Animation_frame_changed")
	
	
	# wait position
	animation = get_animation_name(self.last_direction, "wait")
	set_animation(animation)
	$AnimatedSprite.play()
	if target.get_current_animation() == "afraid":
		var anim = target.get_animation_name(target.last_direction, "wait")
		target.set_animation(anim)
	
	# Compute damage
	var damages = float(self.weapon.base_damages) * ( 1.0 + (float(self.offense_skill) / 20.0) + float(self.attack_bonus)) 
	var take_damages = target.take_damages(damages)
	if take_damages:
		yield(take_damages, "completed")
	
	# clear bonus effects
	on_bonus_timer_timeout()

func on_Animation_frame_changed(var nb_frames, var target):
	if $AnimatedSprite.frame == nb_frames - 2:
		map.shake_camera(Global.TIME_SHAKE_CAMERA_ATTACK)
		target.touched_animation(2)

#########################  STEAL  ###############################

func can_steal(var target) :
	return (states.can_make_main_action() && self.team != target.team && !self.weapon && target.weapon && self.position_index.distance_to(target.position_index) == 1)

# Overrided in player to add QTE
func got_weapon_stealed() :
	return drop_weapon()

func steal(var target) :
	states.steal_OK()
	self.last_direction = Tools.get_direction_cste_from_vec(target.position_index - self.position_index)
	
	# TODO : Steal animation ?
	
	#LOG
	var s = self.get_name() + " steals " + target.get_name()
	emit_signal("output_signal", s)
	
	# Action
	if !Input.is_joy_known(1):
		self.set_process_input(false)
	var try_avoid_steal = target.got_weapon_stealed()
	var weapon_stealed = null
	if try_avoid_steal is GDScriptFunctionState :
		weapon_stealed = yield(try_avoid_steal, "completed")
	if !Input.is_joy_known(1):
		self.set_process_input(true)
	
	if weapon_stealed :
		self.grab_weapon(weapon_stealed)
		self.states.set(states.STEAL_STATUS)
		return true
	#LOG
	s = target.get_name() + " avoid the steal"
	emit_signal("output_signal", s)
	return false


########################  INTERCEPTION  ###############################################

# process the possible interception position
# A : position of the first player of the pass line
# B : position of the second player of the pass line
# return the position index float 
# (need to be rounded to get the int index position and multiply by interval space of map to get the exact position)
func can_intercept(var A, var B):
	A = map.get_cell_center_position(A)
	B = map.get_cell_center_position(B)
	var C = map.get_cell_center_position(self.position_index)
	if (C - A).dot(B - A) > 0 && (C - B).dot(A - B) > 0:
		var pt = Tools.closest_point_to_line(A, B, C)
		var dist = (pt-C).length()
		if round(dist / map.interval_space) <= interception_dist_max:
			return pt
	return null

func interception(var position_index, var object):
	# Move to interception position
#	if position_index != self.position_index:
	var path = map.process_path(self.position_index, position_index, [], ["Characters"])
	if path and path.size() > 1:
		path.pop_back()
		var animation_move_to_by_path = animation_move_to_by_path(path)
		if animation_move_to_by_path:
			yield(animation_move_to_by_path,"completed")
		map.move(self.position_index, position_index, self.name)
#		var move = move(path)
#		if move:
#			yield(move, "completed")
#	return true

func receive_weapon(var weapon, var bonus, var nb):
	# Teammate get the weapon
	self.grab_weapon(weapon)
	
	MUSE.play_sfx(MUSE.OPTION_SELECT, self.stream_player)
	$VibrationsParticles.show()
	
	# Add attack bonus
	self.attack_bonus = bonus
	self.nb_bonus_pass = nb
	
	$VibrationsParticles.change_scale(Tools.linear_function(nb_bonus_pass*2, 1/4.0, 1))
	$VibrationsParticles.change_tempo(Tools.linear_function(nb_bonus_pass*2, -1/8.0, 1))
	
	var vibration_intensity = float(nb_bonus_pass)/3.0
	if Input.is_joy_known(1) :
		Input.start_joy_vibration(self.team,vibration_intensity,vibration_intensity)
	else :
		Input.start_joy_vibration(0, vibration_intensity, vibration_intensity)
	
	MUSE.play_sfx(MUSE.BONUS, self.stream_player)
	self.bonus_timer.wait_time = max(3.5 - float(nb_bonus_pass), 0.0)
	self.bonus_timer.start()
	
	self.map.shake_camera(self.bonus_timer.wait_time, Global.CAMERA_SHAKE_AMPLITUDE_BONUS_PASS, Global.CAMERA_SHAKE_FREQUENCY_BONUS_PASS)
	
	return true

func on_bonus_timer_timeout() :
	self.attack_bonus = 0
	self.nb_bonus_pass = 0
	$VibrationsParticles.reinit()
	$VibrationsParticles.hide()
	MUSE.stop_sfx(self.stream_player)
	# STOP SHAKING CAMERA 
	map.stop_shake_camera()
	if Input.is_joy_known(1) :
		Input.stop_joy_vibration(self.team)
	else :
		Input.stop_joy_vibration(0)

################################# MOVE #################################################
func can_move(var position_end_index):
	if states.can_move() && map.get_existing_path(position_end_index):#map.is_cell_valid(position_end_index, true, [], ["Characters"]):# && displacement_function.f.call_func(self.position_index, position_end_index, displacement_function.args):
		return true
	return false
	
func move(var path):
	states.move_OK()
	#move step by step and check opportunity attack
	var size = path.size()
	var offset = 0
	var cells = []
	for i in range(size):
		var k = size - 1 - i
		#Except the destination cell, check if there is an oppotunity attack on the path
		if k > 0:
			var direction = (path[k-1] - path[k]).normalized()
			cells = map.get_beside_cells(path[k], direction)
			# if it's the origin cell in the path, check all around
			if k == size - 1:
				cells.append(map.get_behind_cell(path[k], -direction))
		var index_cell = get_opportunity_attack(cells)
		#if opportunity attack, stop the move of the player
		if index_cell >= 0:
			offset = size - i
			break
	for i in range(offset):
		path.pop_front()
	path.pop_back()
	if path.size():
		var position_from = self.position_index
		var position_to = path.front()
		var animation_move_to_by_path = animation_move_to_by_path(path)
		
		if animation_move_to_by_path:
			yield(animation_move_to_by_path,"completed")
		map.move(self.position_index, position_to, self.name)
#	else:
#		emit_signal("finish_animation_move_signal")
		
		
################################# PASS #################################################
class DistSorter:
	static func sort(a, b):
		if a["dist"] < b["dist"]:
			return true
		return false

func can_pass():
	if self.weapon && states.can_make_main_action():
		return true
	return false

# When the player confirm for passing (LT pushed and X pushed on the XBOX controller), this function is called
func pass_object_action():
	self.set_process_input(false)
	
	states.pass_OK()
	
	#LOG
	var s = self.get_name() + " pass to " + teammate_for_pass.get_name()
	emit_signal("output_signal", s)
	
	# TODO : if the size of the player with the mark is higher than 1, there is problem
	var weapon = drop_weapon()
	if weapon:
		# compute attack bonus
		var bonus = float(self.attack_bonus) + (float(self.teammate_skill) / 20.0)
		var nb_pass = self.nb_bonus_pass + 1
		
		# pass the weapon
		self.bonus_timer.stop()
		on_bonus_timer_timeout()
		
		MUSE.play_sfx(MUSE.PASS, self.stream_player)
		
		
		
		#animation pass for the character
		var dir = (self.teammate_for_pass.position_index - self.position_index).normalized()
		var animation = get_animation_name(Tools.get_direction_cste_from_vec(dir), "pass")
		set_animation(animation)
#			yield($AnimatedSprite, "animation_finished")
#			animation = get_animation_name(Tools.get_direction_cste_from_vec(dir), "wait")
#			set_animation(animation)
		
		# Stop de bonus for the current player (he will transfer the bonus to the teammate if the pass success)
		self.bonus_timer.stop()
		if Input.is_joy_known(1) :
			Input.stop_joy_vibration(self.team)
		else :
			Input.stop_joy_vibration(0)
		
		#add the graphics of the weapon to the map
		map.add_graphics_obj(weapon, self.position_index)
		
		#check if interception
		var interceptors = []
		var characters = self.turn_handler.characters
		for character in characters:
			if character.team != self.team:
				# TODO : make a better division of the weapon deplacement (for diagonal especially)
				var interception_position = character.can_intercept(self.position_index, teammate_for_pass.position_index)
				# if an interception is possible
				if interception_position:
					interceptors.append({"dist": interception_position.distance_to(self.position), "character": character, "pos": interception_position})
		
		# Lets animate some shit
		var intercepted = false
		var last_pos = self.position
		# Sort interceptions by distance from the beginning of the pass
		interceptors.sort_custom(DistSorter, "sort")
		for interceptor in interceptors :
			# the closest cell of the interception point
			var index_pos_interceptor = map.get_position_index_from_absolute_position(interceptor["pos"])
			# the intersection point
			var pos_interceptor = interceptor["pos"]
			
			# compute weapon deplacement duration
			var vect = pos_interceptor - last_pos
			var vect_dist = vect.length()
			# duration = dist1 / speed+ dist2 / speed … 
			var vect_duration = (vect_dist/Global.PASS_SPEED)
			
			# move weapon to interception point (if a deplacement is needed)
			if pos_interceptor != weapon.position:
				weapon.animation_move_to(weapon.position, pos_interceptor, vect_duration)
				weapon.active_ghost()
				yield(weapon.get_node("Tween"), "tween_completed")
				weapon.disable_ghost()
				
			# Check if the interceptor success and move him
			var interception = interceptor["character"].interception(index_pos_interceptor, weapon)
			if interception is GDScriptFunctionState:
				intercepted = yield(interception, "completed")
			else:
				intercepted = interception

			# pass the turn to the interceptor
			if intercepted :
				#LOG
				s = interceptor["character"].get_name() + " intercepts the weapon !"
				emit_signal("output_signal", s)
				
				#MUSE.play_sfx(MUSE.INTERCEPTION)
				
				#remove the obj from the map
				map.remove_graphics_obj(weapon)
				
				# Interceptor grabs the weapon
				interceptor["character"].grab_weapon(weapon)
				
				#update animation passers
				animation = get_animation_name(Tools.get_direction_cste_from_vec(dir), "wait")
				set_animation(animation)
				animation = teammate_for_pass.get_animation_name(Tools.get_direction_cste_from_vec(-dir), "wait")
				teammate_for_pass.set_animation(animation)
				
				#waiting for other players
#					emit_signal("player_intercepted_by_signal", self, interceptor["character"])
				
				end_turn(interceptor["character"], self.states.AFTER_INTERCEPT_STATUS)
				return 
				#yield(self.turn_handler, "continue_signal")
				
				#no need to test the other interceptors
#					break
				
			last_pos = pos_interceptor
		
		if not intercepted :
			# compute weapon deplacement duration
			var vect = teammate_for_pass.position - last_pos
			var vect_dist = vect.length()
			# duration = dist / speed 
			var vect_duration = vect_dist / Global.PASS_SPEED
			
			# finish animation weapon
			weapon.animation_move_to(weapon.position, self.map.get_cell_center_position(teammate_for_pass.position_index), vect_duration)
			weapon.active_ghost()
			yield(weapon.get_node("Tween"), "tween_completed")
			weapon.disable_ghost()
			
			
			var pass_received = false
			var receive_pass = teammate_for_pass.receive_weapon(weapon, bonus, nb_pass)
			
			# Update own animation and data
			$VibrationsParticles.hide()
			
			
			if receive_pass is GDScriptFunctionState:
				pass_received = yield(receive_pass, "completed")
			else:
				pass_received = receive_pass
				
			#give the turn to the player received the pass
			if pass_received :
				map.remove_graphics_obj(weapon)
#				teammate_for_pass.get_node("MarkPass").set_visible(false)

				#update animation passers
				animation = get_animation_name(Tools.get_direction_cste_from_vec(dir), "wait")
				set_animation(animation)
				
#					emit_signal("player_pass_to_signal", self, teammate_for_pass)
				end_turn(teammate_for_pass, self.states.GET_PASS_STATUS)
				return
				
				# waiting for other players
				#yield(self.turn_handler, "continue_signal")
			# If the reception fail
			else:
				map.add(weapon, teammate_for_pass.position_index)
				
		#update animation passers
		animation = get_animation_name(Tools.get_direction_cste_from_vec(dir), "wait")
		set_animation(animation)
		animation = teammate_for_pass.get_animation_name(Tools.get_direction_cste_from_vec(-dir), "wait")
		teammate_for_pass.set_animation(animation)
				
		if !Input.is_joy_known(1):
			self.set_process_input(true)
		
		get_tree().set_input_as_handled()
		end_turn()
	
func animation_move_to_by_path(var path):
	var size = path.size()
	for i in range(size):
		var k = size - i - 1
		var direction
		if k != size - 1:
			direction = path[k] - path[k+1]
		else:
			direction = path[k] - self.position_index
		$RunParticles.emitting = true
		MUSE.play_sfx(MUSE.WALK, self.stream_player)
		animation_move_to(self.map.get_cell_center_position(path[k]), direction)
		yield($Tween, "tween_completed")
		$RunParticles.emitting = false
	
	var direction
	if path.size() > 1:
		direction = path[path.size()-2] - path[path.size()-1]
	else:
		direction = Vector2(0,1)
	self.last_direction = Tools.get_direction_cste_from_vec(direction)
	var animation = get_animation_name(self.last_direction, "wait")
	set_animation(animation)
#	emit_signal("finish_animation_move_signal")
		
func animation_move_to(var position, var direction = Vector2(1,0)):
	var tween = $Tween
	tween.interpolate_method(self, "set_position", self.position, position, 0.5, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	self.last_direction = Tools.get_direction_cste_from_vec(direction)
	var animation = get_animation_name(self.last_direction, "walk")
	set_animation(animation)
	tween.start()
	
func get_animation_name(var DIR, var name, var with_weapon = true):
	if with_weapon && self.weapon && self.weapon.is_in_group("Swords"):
		return Tools.get_action_from_dir(name + "_sword", DIR)
	elif with_weapon && self.weapon && self.weapon.is_in_group("Bows"):
		return Tools.get_action_from_dir(name + "_bow", DIR)
	else:
		return Tools.get_action_from_dir(name, DIR)

func die():
	MUSE.play_sfx(MUSE.DEATH, self.stream_player)
	yield(self.stream_player, "finished")
	if self.weapon:
		drop_weapon_on_the_floor()
	map.remove_graphics_obj(self)
	map.remove(self)
	emit_signal("die_signal", self)
	
func set_turn_sprite(var sprite):
	turn_sprite = sprite

func clear_teammate():
	if self.teammate_for_pass:
		var animation = self.teammate_for_pass.get_animation_name(self.teammate_for_pass.last_direction, "wait")
		self.teammate_for_pass.set_animation(animation)
#		self.teammate_for_pass.get_node("MarkPass").set_visible(false)
		self.teammate_for_pass = null
		
func end_turn(var charac, var state):
	self.states.reset()
		
	var animation = get_animation_name(last_direction, "wait")
	set_animation(animation)
	
	var s = self.get_name() + " finishes his turn"
	emit_signal("output_signal", s)
	
	emit_signal("end_turn_signal", [charac, state])
		
	
func update_bonus():
	var anim = $AnimatedSprite.get_animation()
	if anim:
		var texture = $AnimatedSprite.get_sprite_frames().get_frame(anim, 0)
		if texture:
			$VibrationsParticles.set_texture(texture)
			
func set_animation(var name):
	$AnimatedSprite.set_animation(name)
	update_bonus()
	
func get_current_animation(var with_weapon = false, var with_direction = false):
	var animation = $AnimatedSprite.get_animation()
	var names = animation.split("_")
	if !with_direction && (names[names.size()-1] == "right" || names[names.size()-1] == "left" || names[names.size()-1] == "up" || names[names.size()-1] == "down"):
		if !with_weapon && (names[names.size() - 2] == "sword" || names[names.size() - 2] == "bow"):
			var s
			for i in names.size() - 3:
				s += names[i]
			return s
		var s
		for i in names.size() - 2:
			s += names[i]
		return s
	return animation
			
	

func set_team(var t):
	team = t
	if team == Global.TEAM_0:
		set_color_border(Global.COLOR_TEAM_0)
	elif team == Global.TEAM_1:
		set_color_border(Global.COLOR_TEAM_1)
		
# expect a vec4
func set_color_border(var color):
	if $AnimatedSprite.get_material():
		$AnimatedSprite.get_material().set_shader_param("u_color", color)
		
func get_opportunity_attack(var cells):
	for i in range(cells.size()):
		var enemi = map.get_obj_by_group(cells[i], "Enemis")
		if enemi && enemi.opportunity_attack():
			enemi.attack(self)
			return i
	return -1
	
func touched_animation(var nb_times):
	for i in nb_times:
		$Tween.interpolate_method($AnimatedSprite, "set_modulate", Color(1,1,1,1), Color(1,0,0,1), 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$Tween.start()
		yield($Tween, "tween_completed")
		$Tween.interpolate_method($AnimatedSprite, "set_modulate", Color(1,0,0,1), Color(1,1,1,1), 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		yield($Tween, "tween_completed")
		$Tween.start()
	
func show_target_symbol(var b):
	$Target.set_visible(b)
	
func set_animation_pass(var b, var current_player_dir):
	if b:
		#put animation to all teammates for getting pass
		for character in self.turn_handler.characters:
			if character == self:
				var animation = get_animation_name(Tools.get_direction_cste_from_vec(current_player_dir), "pass")
				self.set_animation(animation)
			elif character.team == self.team:
				var animation = get_animation_name(Tools.get_direction_cste_from_vec((self.position - character.position).normalized()), "get_pass")
				character.set_animation(animation)
	else:
		#put animation to all teammates for getting pass
		for character in self.turn_handler.characters:
			var animation = get_animation_name(Tools.get_direction_cste_from_vec(character.last_direction), "wait")
			if character.team == self.team:
				character.set_animation(animation)