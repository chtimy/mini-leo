extends "res://fight_mode/script/weapon.gd"

func _init():
	self.function_range = {"f" : funcref(Tools, "circle_area_func"), "args" : [1]}
	set_name("Sword")

func active_ghost():
	$GhostParticles.emitting = true
	$GhostParticles.set_visible(true)
	
func disable_ghost():
	$GhostParticles.emitting = false
	$GhostParticles.set_visible(false)