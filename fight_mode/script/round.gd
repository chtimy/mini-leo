extends Node2D

signal continue_signal
signal cancel_signal
signal output_signal
signal end_game_signal

var minileo_scene = preload("res://fight_mode/scenes/MiniLeo.tscn")
var blond_scene = preload("res://fight_mode/scenes/Blond.tscn")
var GB_scene = preload("res://fight_mode/scenes/GB.tscn")
var jerem_scene = preload("res://fight_mode/scenes/Jerem.tscn")
var bow_scene = preload("res://fight_mode/scenes/Bow.tscn")
var sword_scene = preload("res://fight_mode/scenes/Sword.tscn")

var turns = []
var current_turn = 0
var temporary_player_turn = []
var i_player_choose
enum{NO_MODE, CHOOSE_TURN_MODE}
enum{IA_GAME, PLAYER_GAME}
var mode = CHOOSE_TURN_MODE
var game_mode = PLAYER_GAME

var tmp_selection_turns

var current_teams = null

var characters = []

func _ready():
	if !Input.is_joy_known(1) :
		debug()
	connect("cancel_signal", $MapFight, "cancel")
	connect("output_signal", $Output, "log_message")
	
	if self.game_mode == IA_GAME:
		tmp_selection_turns = {"free_players" : [], "turns_list" : []}
	else:
		tmp_selection_turns = [{"free_players" : [], "turns_list" : []}, {"free_players" : [], "turns_list" : []}]
	
	MUSE.play_background_music(MUSE.BATTLE)
	
func debug():
	var screen_size = OS.get_screen_size(0)
	var window_size = OS.get_window_size()
	OS.set_window_position(screen_size*0.5 - window_size*0.5)
	

	randomize()
	
	var sword = sword_scene.instance()
	var mini_leo = minileo_scene.instance()
	var position_index = Vector2(0,0)
	mini_leo.set_team(Global.TEAM_0)
	mini_leo.set_name("Mini-Leo")
	$MapFight.add_graphics_obj(mini_leo, mini_leo.position_index)
	$MapFight.add(mini_leo, mini_leo.position_index)
	mini_leo.grab_weapon(sword)
	mini_leo.set_animation("wait_sword_down")
	self.characters.append(mini_leo)
	
	var blond = blond_scene.instance()
	position_index = Vector2(0,2)
	blond.set_team(Global.TEAM_0)
	blond.set_name("Blond")
	$MapFight.add_graphics_obj(blond, position_index)
	$MapFight.add(blond, position_index)
	#blond.set_turn_sprite("res://fight_mode/assets/Sprites/HUD/turns/blond.png")
	blond.set_animation("wait_down")
	self.characters.append(blond)
	
	var GB = GB_scene.instance()
	position_index = Vector2(0,4)
	GB.set_team(Global.TEAM_0)
	GB.set_name("GB")
	$MapFight.add_graphics_obj(GB, position_index)
	$MapFight.add(GB, position_index)
	#GB.set_turn_sprite("res://fight_mode/assets/Sprites/HUD/turns/GB.png")
	GB.set_animation("wait_down")
	self.characters.append(GB)
	
	var sword2 = sword_scene.instance()
	var jerem = jerem_scene.instance()
	position_index = Vector2(4,0)
	jerem.set_team(Global.TEAM_1)
	jerem.set_name("Jerem")
	$MapFight.add_graphics_obj(jerem, position_index)
	$MapFight.add(jerem, position_index)
	#jerem.set_turn_sprite("res://fight_mode/assets/Sprites/HUD/turns/jerem.png")
	jerem.grab_weapon(sword2)
#	enemi.interception_dist_max = 2
	jerem.set_animation("wait_sword_down")

	self.characters.append(jerem)
	
	var gouin = minileo_scene.instance()
	position_index = Vector2(1,1)
	gouin.set_team(Global.TEAM_1)
	gouin.set_name("Gouin")
	$MapFight.add_graphics_obj(gouin, position_index)
	$MapFight.add(gouin, position_index)
	#gouin.set_turn_sprite("res://fight_mode/assets/Sprites/HUD/turns/gouin.png")
	gouin.set_animation("wait_down")
	self.characters.append(gouin)
	
	var dono = GB_scene.instance()
	position_index = Vector2(4,4)
	dono.set_team(Global.TEAM_1)
	dono.set_name("Dono")
	$MapFight.add_graphics_obj(dono, position_index)
	$MapFight.add(dono, position_index)
	#dono.set_turn_sprite("res://fight_mode/assets/Sprites/HUD/turns/dono.png")
	dono.set_animation("wait_down")
	self.characters.append(dono)

	var bow = bow_scene.instance()
	bow.position_index = Vector2(1,1)
	$MapFight.add_graphics_obj(bow, bow.position_index)
	$MapFight.add(bow, bow.position_index)
	
#	$MapFight.print_state_map()
	
	var characters = get_tree().get_nodes_in_group("Characters")

	for character in characters:
		character.connect("end_turn_signal", self, "next_turn", [], CONNECT_DEFERRED)
		character.connect("output_signal", $Output, "log_message")
#		character.connect("player_pass_to_signal", self, "player_pass_to", [])
#		character.connect("player_intercepted_by_signal", self, "player_intercepted_by", [])
		# a mettre pour tous les movable pas seulement ici
#		character.connect("change_object_position_signal", $MapFight, "move")
		character.connect("die_signal", self, "character_die")
	
	#	deal_turns(characters.size())
	random_turns(characters.size())
	

func init_game(var teams) :
#	clear_map()
	
	self.current_teams = teams
	
	# set objects on map
	var sword = sword_scene.instance()
	sword.position_index = Vector2(4,3)
	var bow01 = bow_scene.instance()
	bow01.position_index = Vector2(0,3)
	var bow02 = bow_scene.instance()
	bow02.position_index = Vector2(8,3)
	for obj in [sword, bow01, bow02] :
		$MapFight.add_graphics_obj(obj, obj.position_index)
		$MapFight.add(obj, obj.position_index)
	
	# Set red team (left)
	for i in teams.size() :
		for j in teams[i].size() :
			var scene = load(teams[i][j])
			var character = scene.instance()
			character.set_team(i)
			if i : # == TEAM BLUE
				character.set_animation("wait_left")
			else : # TEAM RED
				character.set_animation("wait_right")
			
			self.characters.append(character)
			
			# Define position
			var position_index = null
			match j :
				0 :
					position_index = Vector2(1 + 6*i, 1)
				1 :
					position_index = Vector2(3 + 2*i,3)
				2 :
					position_index = Vector2(1 + 6*i, 5)
			
			$MapFight.add_graphics_obj(character, position_index)
			$MapFight.add(character, position_index)
			
			# Connections
			character.connect("end_turn_signal", self, "next_turn", [], CONNECT_DEFERRED)
			character.connect("output_signal", $Output, "log_message")
			character.connect("die_signal", self, "character_die")
	
	# Finish launch
	if self.game_mode == IA_GAME:
		tmp_selection_turns = {"free_players" : [], "turns_list" : []}
	else:
		tmp_selection_turns = [{"free_players" : [], "turns_list" : []}, {"free_players" : [], "turns_list" : []}]
	
	random_turns(characters.size())
	MUSE.play_background_music(MUSE.BATTLE)

func clear_map() :
	# for each object on map except panier
	var map = get_node("MapFight")
	var obj_on_map = get_node("MapFight/YSort").get_children()
	for obj in obj_on_map :
		if obj.get_name() != "panier" :
			map.remove_graphics_obj(obj)
			map.remove(obj)
			if obj.is_in_group("Characters") :
				clear_character(obj)
			else :
				call_deferred("queue_free", obj)


func random_turns(var nb_characters):
	self.turns.clear()
	var enemis = get_tree().get_nodes_in_group("Enemis")
	var players = get_tree().get_nodes_in_group("Players")
	var t = []
	for i in nb_characters:
		t.append(i)
	Tools.randomize_array(t)
	for i in enemis.size():
		self.turns.append({"character" : enemis[i], "turn" : t[i]})
	var k = enemis.size()
	for i in players.size():
		self.turns.append({"character" : players[i], "turn" : t[i+k]})
	set_fight_mode()
	
	
#func deal_turns(var nb_characters):
#	self.turns.clear()
#
#	var random_turns = process_initial_turns(nb_characters)
#	var enemis = get_tree().get_nodes_in_group("Enemis")
#	var players = get_tree().get_nodes_in_group("Players")
#
#	if self.game_mode == IA_GAME:
#		# deal turns for enemis
#		#IA Mode
#		var i = 0
#		for enemi in enemis:
#			self.turns.append({"character" : enemi, "turn" : random_turns[i]})
#			i+=1
#		# deal for players
#		for j in range(players.size()):
#			self.tmp_selection_turns.turns_list.append({"player" : null, "turn" : random_turns[i+j]})
#			self.tmp_selection_turns.free_players.append(players[j])
#		self.tmp_selection_turns.turns_list.sort_custom(Tools.CustomSort, "sort_turns")
#	else:
#		#Players Mode
#		for j in range(enemis.size()):
#			self.tmp_selection_turns[0].turns_list.append({"player" : null, "turn" : random_turns[j]})
#			self.tmp_selection_turns[0].free_players.append(enemis[j])
#		var i = enemis.size()
#		# deal for players
#		for j in range(players.size()):
#			self.tmp_selection_turns[1].turns_list.append({"player" : null, "turn" : random_turns[i+j]})
#			self.tmp_selection_turns[1].free_players.append(players[j])
#		self.tmp_selection_turns[0].turns_list.sort_custom(Tools.CustomSort, "sort_turns")
#		self.tmp_selection_turns[1].turns_list.sort_custom(Tools.CustomSort, "sort_turns")
#
#	set_choose_turn_mode(true, self.tmp_selection_turns.free_players)

#func _input(event):
#	if self.mode == CHOOSE_TURN_MODE:
#		if event.is_action_pressed("ui_right"):
#			self.i_player_choose += 1
#			if self.i_player_choose == self.tmp_selection_turns.free_players.size():
#				self.i_player_choose = 0
#			$MapFight.move_cursor_to(self.tmp_selection_turns.free_players[self.i_player_choose].position_index)
#		if event.is_action_pressed("ui_left"):
#			self.i_player_choose -= 1
#			if self.i_player_choose == -1:
#				self.i_player_choose = self.tmp_selection_turns.free_players.size()-1
#			$MapFight.move_cursor_to(self.tmp_selection_turns.free_players[self.i_player_choose].position_index)
#			# player is the next to play in the order of turns
#		if event.is_action_pressed("ui_accept"):
#			var player = self.tmp_selection_turns.free_players[self.i_player_choose]
#			self.tmp_selection_turns.free_players.remove(self.i_player_choose)
#			for turn in self.tmp_selection_turns.turns_list:
#				if !turn.player:
#					turn.player = player
#					break
#			if !self.tmp_selection_turns.free_players.empty():
#				self.i_player_choose -= 1
#				self.i_player_choose = max(0, self.i_player_choose)
#				$MapFight.move_cursor_to(self.tmp_selection_turns.free_players[self.i_player_choose].position_index)				
#			else:
#				set_choose_turn_mode(false)
#				for tmp_turn in self.tmp_selection_turns.turns_list:
#					self.turns.append({"character" : tmp_turn.player, "turn" : tmp_turn.turn})
#				set_fight_mode(true)
#		# last player selectionned becomes free_player
#		if event.is_action_pressed("ui_cancel"):
#			for turn in self.tmp_selection_turns.turns_list:
#				if turn.player:
#					var player = turn.player
#					turn.player = null
#					self.tmp_selection_turns.free_players.append(player)
#	if event.is_action_pressed("ui_cancel"):
#		emit_signal("cancel_signal")
#		get_tree().set_input_as_handled()
		
func set_choose_turn_mode(var boolean, var players = null, var enemis = null):
	$MapFight.set_cursor_vibility(boolean)
	if boolean:
		self.mode = CHOOSE_TURN_MODE
		$MapFight.set_cursor_vibility(boolean)
		if !players:
			players = get_tree().get_nodes_in_group("Players")
		if self.game_mode == IA_GAME:
			#index of the free player
			self.i_player_choose = 0
			$MapFight.set_cursor_vibility(boolean)
			$MapFight.move_cursor_to(players[self.i_player_choose].position_index)
		else:
			if !enemis:
				enemis = get_tree().get_nodes_in_group("Enemis")
			#index of the free player
			self.i_player_choose = [0,0]
#			for i in 
			$MapFight.move_cursor_to(players[self.i_player_choose].position_index)
	else:
		self.mode = NO_MODE
	set_process_input(boolean)
	
	
func set_fight_mode():
#	self.mode = NO_MODE
	self.turns.sort_custom(Tools.CustomSort, "sort_turns")
	$HUD.init_turns(self.turns)
	self.turns.front().character.play()

func next_turn(var character = null):
	#if self.temporary_player_turn.empty():
	var obj = self.turns.pop_front()
	self.turns.push_back(obj)
	$HUD.switch_turn(0, self.turns.size()-1)
	
	if character[0]:
		var pos = push_front(character[0])
	
	self.turns.front().character.play()
		
#	else:
#		var player = self.temporary_player_turn.front()
#		self.temporary_player_turn.clear()
#	#		self.temporary_player_turn.pop_front()
#		player.play(player.states.AFTER_PASS_STATUS)
#		player.end_turn()

func push_front(var character):
	var i = 0
	var removed_turn
	for turn in self.turns:
		if turn.character == character:
			removed_turn = turns[i]
			turns.remove(i)
			break
		i+=1
	self.turns.push_front(removed_turn)
	$HUD.switch_turn(i, 0)

func process_initial_turns(var nb):
	var turns = []
	turns.resize(nb)
	for i in range(nb):
		turns[i] = i
	return Tools.randomize_array(turns)
	
#func player_pass_to(var player_from, var player_to):
#	var s = "Player : " + player_from.get_name() + " passes to " + player_to.get_name()
#	emit_signal("output_signal", s)
#	#self.temporary_player_turn.push_back(player_from)
##	player_from.set_enable(false)
##	player_from.clear()
##	player_from.states.reset(false)
#	player_to.play(player_to.states.GET_PASS_STATUS)

#func player_intercepted_by(var player_from, var player_by):
#	#self.temporary_player_turn.push_back(player_from)
##	player_from.set_enable(false)
##	player_from.clear()
##	player_from.states.reset(false)
#	player_by.play(player_by.states.AFTER_INTERCEPT_STATUS)

func clear_character(var character) :
	# remove the player from the turns collector
	var k = -1
	for i in self.turns.size():
		if turns[i].character == character:
			k = i
	if k == -1:
		Tools.print_error("Impossible to remove the player from the turns collector")
	turns.remove(k)
	$HUD.remove(k)
	
	#remove the player from the temporary turns if it's possible
	k = -1
	for i in self.temporary_player_turn.size():
		if temporary_player_turn[i] == character:
			k = i
	if k != -1:
		temporary_player_turn.remove(k)
	
	# remove from character list
	k = -1
	for i in self.characters.size() :
		if self.characters[i] == character :
			k = i
			break
	if k != -1 :
		self.characters.remove(k)
		
	call_deferred("queue_free", character)

func character_die(var character):
	# get the team of the character
	var team = character.team
	
	# Clear character
	clear_character(character)
	
	# check if all the team die
	if all_team_die(team) :
		emit_signal("end_game_signal", self.current_teams, self.characters[0].team)

func all_team_die(var team) :
	for c in self.characters :
		if c.team == team :
			return false
	
	return true

#func _on_MapFight_select_cell(var position):
#	self.characters[self.turns[self.current_turn]].selectyion_cell(position)