extends Control

var timer = Timer.new()
var device = 0
var input = "ui_accept"
var sprite_up = "A_Button"
var sprite_down = "A_ButtonPressed"

signal qte_completed_signal

func _ready():
	# hide itself
	self.set_visible(false)
	
	# No input as default
	set_process_input(false)
	set_process(false)
	
	# QTE timer init
	self.timer.wait_time = 1
	self.timer.one_shot = true
	self.timer.connect("timeout", self, "on_timer_timeout")
	add_child(self.timer)

# process will update texture progress based on timer
func _process(delta):
	var time_passed = 360 * (self.timer.wait_time - self.timer.time_left) / self.timer.wait_time
	var texture_progress = self.get_node("TextureProgress")
	if texture_progress.value != time_passed :
		texture_progress.value = time_passed

func _input(event):
	if !Input.is_joy_known(1) || event.device == device :
		# if valid button
		if event.is_action_pressed("ui_accept") or event.is_action_pressed("ui_select") or event.is_action_pressed("ui_cancel") or event.is_action_pressed("ui_info"):
			# If before end of the timer
			if self.timer.time_left > 0 :
				# Stop timer (and process just in case)
				self.timer.stop()
				set_process(false)
				
				# button pressed
				self.get_node(self.sprite_down).set_visible(true)
				self.get_node(self.sprite_up).set_visible(false)
				
				# Get value of texture progress
				var texture_progress = self.get_node("TextureProgress")
				var timing = self.get_node("TextureProgress2")
				
				# Determining if right timing or not
				var right_timing = texture_progress.value >= 360 - timing.radial_initial_angle and texture_progress.value <= 360 - timing.radial_initial_angle + timing.value
				
				# Wrong input or wrong timing
				if not right_timing or not event.is_action_pressed(self.input):
					emit_signal("qte_completed_signal", 1)
				# Good input right in time
				else :
					emit_signal("qte_completed_signal", 2)

func on_timer_timeout():
	emit_signal("qte_completed_signal", 0)

func init_input(var input):
	
	match input :
		"ui_accept":
			self.sprite_up = "A_Button"
			self.sprite_down = "A_ButtonPressed"
		"ui_cancel":
			self.sprite_up = "B_Button"
			self.sprite_down = "B_ButtonPressed"
		"ui_info":
			self.sprite_up = "Y_Button"
			self.sprite_down = "Y_ButtonPressed"
		"ui_select":
			self.sprite_up = "X_Button"
			self.sprite_down = "X_ButtonPressed"
		_: #default
			return false
	
	self.input = input
	
	return true

# Use random qte if you don't need a specific input
func launch_qte(var device, var input, var skill, var duration = 1) :
	# init input
	if not init_input(input) :
		var e = "Input " + input + "is unknown"
		Tools.print_error(e)
		return null
	
	# Compute texture progress 2 value
	self.get_node("TextureProgress2").value = (float(skill) / 100.0) * 360.0 
	
	# Show itself
	self.get_node(self.sprite_up).set_visible(true)
	self.get_node(self.sprite_down).set_visible(false)
	self.set_visible(true)
	
	self.device = device
	set_process_input(true)
	set_process(true)
	self.timer.wait_time = duration
	self.timer.start()
	var result = yield(self, "qte_completed_signal")
	
	set_process(false)
	set_process_input(false)
	
	# wait a moment to see qte animation
	if result > 0 :
		var t = Timer.new()
		t.set_wait_time(0.1)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t, "timeout")
		t.queue_free()
	
	self.set_visible(false)
	# just in case
	self.get_node(self.sprite_up).set_visible(false)
	self.get_node(self.sprite_down).set_visible(false)
	
	return result

func launch_random_qte(var device, var skill, var duration = 1) :
	# Respectively A, B, X and Y on a Xbox controller
	var actions = ["ui_accept", "ui_select", "ui_info"]
	
	var qte = launch_qte(device, actions[randi() % actions.size()], skill, duration)
	if qte :
		return yield(qte, "completed")