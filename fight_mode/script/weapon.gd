extends "res://fight_mode/script/movable.gd"

var base_damages = 20
var function_range = {"f" : funcref(Tools, "circle_area_func"), "args" : [1]}

func _ready():
	var tween = Tween.new()
	tween.set_name("Tween")
	add_child(tween)
	add_to_group("Objects")
	
func update_graphics_position_from_index(var position_index):
	set_position(self.map.get_cell_center_position(position_index) - Vector2(0,1))