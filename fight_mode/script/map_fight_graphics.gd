extends "res://fight_mode/script/map_fight.gd"

export (int) var interval_space
var graphics_arrow_turn_scene = load("res://fight_mode/scenes/HUD/Arrow_turn.tscn")
var graphics_Button_HUD_scene = preload("res://fight_mode/scenes/HUD/ButtonHUD.tscn")

signal select_cell_signal

#TMP
enum{DRAW_ACTIVE_CELLS, DRAW_ACTIVE_CELL_CURSOR}
var states = [false, false]
var active_cells = []
var position_cursor = Vector2(0,0)
var arrow_move = {"polygon" : [], "draw" : false}

var graphics_arrow_turn = graphics_arrow_turn_scene.instance()


func _ready():
	Global.camera_parameters.offset = get_tree().get_root().get_viewport().size / 2 - get_position()
	$Camera2D.set_offset(Global.camera_parameters.offset)
	$Camera2D.init_offset = Global.camera_parameters.offset
	$Camera2D/BlurRect.rect_size = get_viewport_rect().size
	$Camera2D/BlurRect.rect_position = -self.position
	
	scale_graphics_arrow(0.5)
	
	var indicator = add_indicator(Vector2(0,0), "A", "", "move_indicator")
	indicator.rect_scale = Vector2(0.5,0.5)
	indicator.hide()
	indicator = add_indicator(Vector2(0,0), "X", "grab", "grab_indicator")
	indicator.rect_scale = Vector2(0.5,0.5)
	indicator.offset = Vector2(interval_space*0.5, interval_space*0.25)
	indicator.hide()
	indicator = add_indicator(Vector2(0,0), "X", "steal", "steal_indicator")
	indicator.rect_scale = Vector2(0.5,0.5)
	indicator.offset = Vector2(interval_space*0.5, interval_space*0.25)
	indicator.hide()
	indicator = add_indicator(Vector2(0,0), "X", "sword", "attack_sword_indicator")
	indicator.rect_scale = Vector2(0.5,0.5)
	indicator.offset = Vector2(interval_space*0.5, interval_space*0.25)
	indicator.hide()
	indicator = add_indicator(Vector2(0,0), "X", "bow", "attack_bow_indicator")
	indicator.rect_scale = Vector2(0.5,0.5)
	indicator.offset = Vector2(interval_space*0.5, interval_space*0.25)
	indicator.hide()
	indicator = add_indicator(Vector2(0,0), "combineRTJoy", "", "pass_indicator")
	indicator.rect_scale = Vector2(0.5,0.5)
	indicator.offset = Vector2(-interval_space*0.5, -interval_space*0.25)
	indicator.hide()

#func _unhandled_input(event):
#	if states[DRAW_ACTIVE_CELL_CURSOR]:
#		if Input.is_action_pressed("ui_right"):
#			self.position_cursor = Vector2(clamp(position_cursor.x+1, 0, size_mat.x), position_cursor.y)
#		elif Input.is_action_pressed("ui_left"):
#			self.position_cursor = Vector2(clamp(position_cursor.x-1, 0, size_mat.x), position_cursor.y)
#		elif Input.is_action_pressed("ui_down"):
#			self.position_cursor = Vector2(position_cursor.x, clamp(position_cursor.y+1, 0, size_mat.y))
#		elif Input.is_action_pressed("ui_up"):
#			self.position_cursor = Vector2(position_cursor.x, clamp(position_cursor.y-1, 0, size_mat.y))
#		if Input.is_action_pressed("ui_accept"):
#			select_cell()
#		update()
		
func move_cursor(var direction):
	MUSE.play_sfx(MUSE.CURSOR, null, false)
	if direction == Global.RIGHT:
		self.position_cursor = Vector2(clamp(position_cursor.x+1, 0, size_mat.x-1), position_cursor.y)
	elif direction == Global.LEFT:
		self.position_cursor = Vector2(clamp(position_cursor.x-1, 0, size_mat.x-1), position_cursor.y)
	elif direction == Global.DOWN:
		self.position_cursor = Vector2(position_cursor.x, clamp(position_cursor.y+1, 0, size_mat.y-1))
	elif direction == Global.UP:
		self.position_cursor = Vector2(position_cursor.x, clamp(position_cursor.y-1, 0, size_mat.y-1))
	update()

func move_cursor_to(var position_index):
	self.position_cursor = position_index
	update()
	
func get_cursor_position_index():
	return position_cursor

func _draw():
	var size_max = interval_space * size_mat
	for i in range(self.size_mat.x + 1):
		draw_line(Vector2(i * interval_space, 0), Vector2(i * interval_space, size_max.y), Color(1,1,1, 0.3))
	for j in range(self.size_mat.y + 1):
		draw_line(Vector2(0, j * interval_space), Vector2(size_max.x, j * interval_space), Color(1,1,1, 0.3))
	
	if self.states[DRAW_ACTIVE_CELLS]:
		for cell in self.active_cells:
			var position = get_cell_origin_position(cell.position)
			draw_rect(Rect2(position, Vector2(self.interval_space, self.interval_space)), Color(1,0,0,0.4))
			var c = Color(1,1,1,0.5)
			draw_line(position, position + Vector2(self.interval_space, 0), c)
			draw_line(position + Vector2(self.interval_space, 0), position + Vector2(self.interval_space, self.interval_space), c)
			draw_line(position + Vector2(self.interval_space, self.interval_space), position + Vector2(0, self.interval_space), c)
			draw_line(position + Vector2(0, self.interval_space), position, c)
	if self.states[DRAW_ACTIVE_CELL_CURSOR]:
		var position = get_cell_origin_position(self.position_cursor)
		var c = Color(1,1,0,1)
		var w = 2.0
		draw_line(position, position + Vector2(self.interval_space, 0), c, w)
		draw_line(position + Vector2(self.interval_space, 0), position + Vector2(self.interval_space, self.interval_space), c, w)
		draw_line(position + Vector2(self.interval_space, self.interval_space), position + Vector2(0, self.interval_space), c, w)
		draw_line(position + Vector2(0, self.interval_space), position, c, w)
#		draw_rect(Rect2(position, Vector2(self.interval_space, self.interval_space)), Color(1,1,0), false)
	
	if self.arrow_move.draw:
		var colors = []
		colors.resize(self.arrow_move.polygon.size())
		for i in range(colors.size()):
			colors[i] = Color(0,0,0,0.5)
		draw_polygon(self.arrow_move.polygon, colors)

func get_cell_origin_position(var position_index):
	return position_index * interval_space
	
func get_cell_center_position(var position_index):
	return position_index * self.interval_space + Vector2(self.interval_space * 0.5, self.interval_space * 0.5)

func get_position_from_index(var index_pos):
	return index_pos * self.interval_space
	
func get_position_index_from_absolute_position(var position_absolute):
	return Vector2(int(position_absolute.x) / interval_space, int(position_absolute.y) / interval_space)

func select_cell(): 
	var f = funcref(Tools, "circle_area_func")
#	disable_active_cells()
	for cell in self.active_cells:
		if cell.position == position_cursor:
			emit_signal("select_cell_signal", position_cursor)
			return true
	return false
	
func cancel():
	emit_signal("select_cell_signal", null)
	
# Set the visibility of the move zone around the player, colors cells around
# visible : Boolean The visibility of the zone
# player : Object Player node
func set_move_zone_visibility(var visible):
	states[DRAW_ACTIVE_CELLS] = visible
	update()
	
func process_move_zone(var player):
	self.active_cells.clear()
	var displacement_func = player.displacement_function
	var cells = get_cells(player.position_index, displacement_func.f, displacement_func.args, true, ["Characters"], ["Weapons"])
	for cell in cells:
		var path = process_path(player.position_index, cell, ["Characters"], ["Weapons"])
		if path && path.size() - 1 <= player.nb_move:
			self.active_cells.push_back({"path" : path, "position" : cell})
	
func get_existing_path(var position_index):
	for cell in self.active_cells:
		if cell.position == position_index:
			return cell.path
	return null
	
func nb_existing_path():
	return self.active_cells.size()
	
# Set the visibility of the cursor on map
# visible : Boolean The visibility of the cursor
# position_index : Vector2 Position index of the cursor on the map
func set_cursor_vibility(var visible):
	states[DRAW_ACTIVE_CELL_CURSOR] = visible
	update()
	
		
func set_draw_arrow(var visible, var path = null):
	if visible:
		arrow_move.polygon.clear()
		if path:
			Tools.draw_arrow(arrow_move.polygon, path, interval_space)
			self.arrow_move.draw = true
	else:
		self.arrow_move.draw = false
		hide_indicator("move_indicator")
	update()
	
func add_graphics_obj(var obj, var position_index):
	$YSort.add_child(obj)
	obj.update_graphics_position_from_index(position_index)
	
func remove_graphics_obj(var obj):
	$YSort.remove_child(obj)
	
# ARROW
func focus_character_arrow(var character, var offset):
	unfocus_arrow()
	character.add_child(self.graphics_arrow_turn)
	self.graphics_arrow_turn.start(offset, Vector2(0,15), 1)

func unfocus_arrow():
	if self.graphics_arrow_turn.get_parent():
		self.graphics_arrow_turn.get_parent().remove_child(self.graphics_arrow_turn)
		
func scale_graphics_arrow(var scale):
	self.graphics_arrow_turn.set_scale(Vector2(scale, scale))
	
func add_indicator(var position_index, var name, var type = "", var name_indicator = ""):
	var indicator = graphics_Button_HUD_scene.instance()
	add_child(indicator)
	if name_indicator != "":
		indicator.set_name(name_indicator)
	indicator.set(get_cell_center_position(position_index), name, type)
	return indicator
	
func remove_indicator(var name):
	var node = get_indicator(name)
	if node:
		var parent = node.get_parent()
		if parent:
			parent.call_deferred("remove_child", node)
		node.call_deferred("queue_free")
				
func move_indicator(var name, var position_index):
	var node = get_indicator(name)
	if node:
		node.set_position(get_cell_center_position(position_index) + node.offset)
	
func remove_all_indicators():
	for node in get_tree().get_nodes_in_group("Indicators"):
		var parent = node.get_parent()
		if parent:
			parent.remove_child(node)
			parent.call_deferred("remove_child", node)
			node.call_deferred("queue_free")
			
func hide_indicator(var name):
	var node = get_indicator(name)
	if node:
		node.hide()
		
func hide_all_indicators():
	for node in get_tree().get_nodes_in_group("Indicators"):
		node.hide()

func show_indicator(var name):
	var node = get_indicator(name)
	if node:
		node.show()
		
func get_indicator(var name):
	for node in get_tree().get_nodes_in_group("Indicators"):
		if node.get_name() == name:
			return node
	return null

# CAMERA --------------------
func focus_camera_on(var object, var zoom):
	$Tween.interpolate_property($Camera2D, "offset", $Camera2D.offset, object.position, 0.3, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	zoom_camera(zoom)
	
func zoom_camera(var zoom):
	$Camera2D/BlurRect.show()
	$Tween.interpolate_property($Camera2D, "zoom", $Camera2D.zoom, zoom, 0.3, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	
func unfocus_camera(var object):
	$Tween.interpolate_property($Camera2D, "offset", $Camera2D.offset, Global.camera_parameters.offset, 0.3, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	zoom_camera(Global.camera_parameters.zoom)
	
# intensity : default = 8 ; frequency : default = 15
func shake_camera(var duration, var intensity = 8, var frequency = 15):
	$Camera2D.shake(duration, intensity, frequency)

func stop_shake_camera():
	$Camera2D.stop_shake()

func _on_Tween_tween_completed(object, key):
	print(key)
	print(object)
	print($Camera2D)
	if key == ":zoom" && object == $Camera2D:
		$Camera2D/BlurRect.hide()
