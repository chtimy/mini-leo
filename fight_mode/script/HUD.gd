extends Control

var frame_scene = load("res://fight_mode/scenes/HUD/frame_character_turn.tscn")
#var offset_height
var ref_time = 1.0
var running_tweens = 0
var animations = []
onready var turns_container = $Turns

#func _ready():
#	var minileo_scene = load("res://fight_mode/scenes/MiniLeo.tscn")
#	var blond_scene = load("res://fight_mode/scenes/Blond.tscn")
#	var GB_scene = load("res://fight_mode/scenes/GB.tscn")
#	var jerem_scene = load("res://fight_mode/scenes/Jerem.tscn")
#	var mini_leo = minileo_scene.instance()
#	mini_leo.set_team(Global.TEAM_0)
#	var blond = blond_scene.instance()
#	blond.set_team(Global.TEAM_0)
#	var GB = GB_scene.instance()
#	GB.set_team(Global.TEAM_0)
#	var jerem = jerem_scene.instance()
#	jerem.set_team(Global.TEAM_1)
#	var gouin = minileo_scene.instance()
#	gouin.set_team(Global.TEAM_1)
#	var dono = GB_scene.instance()
#	dono.set_team(Global.TEAM_1)
#	var turns = []
#	turns.push_back({"character" : mini_leo})
#	turns.push_back({"character" : blond})
#	turns.push_back({"character" : GB})
#	turns.push_back({"character" : jerem})
#	turns.push_back({"character" : gouin})
#	turns.push_back({"character" : dono})
#
#	init_turns(turns)
#
#	yield(get_tree(), "idle_frame")
#
#	var next_turn = switch_turn(4, 2)
#	next_turn = switch_turn(0, 5)
#	next_turn = remove(3)
#	next_turn = switch_turn(0, 4)
	

func init_turns(var turns):
	var i = 0
	for turn in turns:
		var frame = frame_scene.instance()
		frame.get_node("Charac").set_texture(turn.character.turn_sprite)
		frame.set_frame_color(Global.TURN_HANDLER_PARAM.color_frame_border)
		if turn.character.team == Global.TEAM_0:
			frame.get_node("BG").set_frame_color(Color(1,0.3,0.3,1.0))
		else:
			frame.get_node("BG").set_frame_color(Color(0.3,0.3,1,1.0))
		self.turns_container.add_child(frame)
#		yield(get_tree(), "idle_frame")
		frame.set_position(Vector2(frame.rect_size.x * i, 0))
		i+=1
	if turns.size():
		var viewport_size = get_tree().get_root().get_viewport().get_size()
		var size = Vector2(self.turns_container.get_children()[0].get_size().x * turns.size(), self.turns_container.get_children()[0].get_size().y)
		var center = Vector2(viewport_size.x / 2 - size.x / 2, 0)
		self.turns_container.set_position(center)
#		self.offset_height = frame.rect_size.y * Global.TURN_HANDLER_PARAM.offset_height
		self.turns_container.get_child(0).set_frame_color(Global.TURN_HANDLER_PARAM.color_frame_border_first)
		
func switch_turn(var from, var to):
	if self.running_tweens:
		self.animations.push_back({"function" : funcref(self, "switch_turn_intern"), "params" : [from, to]})
	else:
		switch_turn_intern([from, to])
	if self.animations.size() > 1:
		$Tween.playback_speed = ref_time * 5.0
	else:
		$Tween.playback_speed = ref_time

func switch_turn_intern(var params):
	var from = params[0]
	var to = params[1]
	var size = self.turns_container.get_children().size()
	if from >= 0 && from < size && to >= 0 && to < size:
		var child = self.turns_container.get_children()[from]
		var child_to = self.turns_container.get_children()[to]
		var pos = child.rect_position
		var destination = child_to.rect_position
		child.set_frame_color(Global.TURN_HANDLER_PARAM.color_frame_border)
		child_to.set_frame_color(Global.TURN_HANDLER_PARAM.color_frame_border)
		$Tween.interpolate_method(child, "set_modulate", child.get_modulate(), Color(1,1,1,0), Global.TURN_HANDLER_PARAM.speed_height, Tween.TRANS_LINEAR,Tween.EASE_OUT_IN)
		start_tween()
		if from < to:
			for i in range(from+1, to+1):
				var other_child = self.turns_container.get_children()[i]
				$Tween.interpolate_method(other_child, "set_position", other_child.rect_position, other_child.rect_position - Vector2(other_child.rect_size.x, 0), Global.TURN_HANDLER_PARAM.speed_width, Tween.TRANS_LINEAR,Tween.EASE_OUT_IN, Global.TURN_HANDLER_PARAM.speed_height)
				start_tween()
		else:
			for i in range(to, from):
				var other_child = self.turns_container.get_children()[i]
				$Tween.interpolate_method(other_child, "set_position", other_child.rect_position, other_child.rect_position + Vector2(other_child.rect_size.x, 0), Global.TURN_HANDLER_PARAM.speed_width, Tween.TRANS_LINEAR,Tween.EASE_OUT_IN, Global.TURN_HANDLER_PARAM.speed_height)
				start_tween()
		$Tween.interpolate_method(child, "set_position", child.rect_position, destination, 0.01,  Tween.TRANS_LINEAR,Tween.EASE_OUT_IN, Global.TURN_HANDLER_PARAM.speed_width + Global.TURN_HANDLER_PARAM.speed_height)
		start_tween()
		$Tween.interpolate_method(child, "set_modulate", Color(1,1,1,0), Color(1,1,1,1), Global.TURN_HANDLER_PARAM.speed_height, Tween.TRANS_LINEAR,Tween.EASE_OUT_IN, Global.TURN_HANDLER_PARAM.speed_width + Global.TURN_HANDLER_PARAM.speed_height)
		start_tween()
		self.turns_container.move_child(child, to)
		self.turns_container.get_children()[0].set_frame_color(Global.TURN_HANDLER_PARAM.color_frame_border_first)


func remove(var k):
	if self.running_tweens:
		self.animations.push_back({"function" : funcref(self, "remove_intern"), "params" : [k]})
	else:
		remove_intern([k])
	if self.animations.size() > 3:
			$Tween.playback_speed = ref_time * 20.0
	elif self.animations.size() > 1:
		$Tween.playback_speed = ref_time * 5.0
	else:
		$Tween.playback_speed = ref_time
		
func remove_intern(var params):
	var k = params[0]
	var size = self.turns_container.get_children().size()
	if k >= 0 && k < size:
		var child = self.turns_container.get_child(k)
		$Tween.interpolate_method(child, "set_modulate", child.get_modulate(), Color(1,1,1,0), Global.TURN_HANDLER_PARAM.speed_height, Tween.TRANS_LINEAR,Tween.EASE_OUT_IN)
		start_tween()
		for i in range(k, size):
			var other_child = self.turns_container.get_children()[i]
			$Tween.interpolate_method(other_child, "set_position", other_child.rect_position, other_child.rect_position - Vector2(other_child.rect_size.x, 0), Global.TURN_HANDLER_PARAM.speed_width, Tween.TRANS_LINEAR,Tween.EASE_OUT_IN)
			start_tween()
		self.turns_container.call_deferred("remove_child", child)
		if self.turns_container.get_children().size():
			self.turns_container.get_child(0).call_deferred("set_frame_color", Global.TURN_HANDLER_PARAM.color_frame_border_first)
		
		
func start_tween():
	self.running_tweens += 1
	$Tween.start()

func _on_Tween_tween_completed(object, key):
	self.running_tweens -= 1
	if self.running_tweens == 0 && self.animations.size():
		if self.animations.size() > 3:
			$Tween.playback_speed = ref_time * 20.0
		elif self.animations.size() > 1:
			$Tween.playback_speed = ref_time * 5.0
		else:
			$Tween.playback_speed = ref_time
		self.animations[0].function.call_func(self.animations[0].params)
		self.animations.pop_front()
