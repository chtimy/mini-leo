extends Control

var minileo_scene = preload("res://fight_mode/scenes/MiniLeo.tscn")
var blond_scene = preload("res://fight_mode/scenes/Blond.tscn")
var GB_scene = preload("res://fight_mode/scenes/GB.tscn")
var Jerem_scene = preload("res://fight_mode/scenes/Jerem.tscn")

var player_list
var teams = [
	{
		"node": "RedTeam",
		"players": [0,0,0],
		"current": 1, # current player selected
		"ready": false,
		"stream": null
	},
	{
		"node": "BlueTeam",
		"players": [0,0,0],
		"current": 1,
		"ready": false,
		"stream": null
	}
]

var device_timers = [Timer.new(),Timer.new()]
var ready_timer = Timer.new()

signal ready_signal

func _ready():
	# Init player list
	player_list = [
		minileo_scene.instance(),
		blond_scene.instance(),
		GB_scene.instance(),
		Jerem_scene.instance()
	]
	
	MUSE.play_background_music(MUSE.TITLE)
	
	# Init players
	for team in teams :
		init_team(team)
	
	# Init timers
	for timer in device_timers :
		timer.one_shot = true
		timer.wait_time = 0.2
		add_child(timer)
	ready_timer.one_shot = true
	ready_timer.wait_time = 5
	ready_timer.connect("timeout", self, "on_ready_timer_timeout")
	add_child(ready_timer)
	
	set_process_input(true)

func _input(event):
	var team = teams[event.device]
	var player_selected = team["current"] 
	
	if event.is_action_pressed("ui_left") || event.is_action_pressed("ui_right") :
		if device_timers[event.device].is_stopped() :
			
			if event.is_action_pressed("ui_left") :
				player_selected -= 1
				if player_selected < 0 :
					player_selected = team["players"].size() - 1
			elif event.is_action_pressed("ui_right") :
				player_selected = (player_selected + 1) % team["players"].size()
			
			MUSE.play_sfx(MUSE.OPTION_SELECT, team["stream"])
			
			update_player_selected(team, player_selected)
			
			device_timers[event.device].start()
	
	if not team["ready"] :
		if event.is_action_pressed("ui_focus_next") || event.is_action_pressed("ui_focus_prev") :
			
			var current = team["players"][player_selected]
			if event.is_action_pressed("ui_focus_next") :
				current = (current + 1) % player_list.size()
			elif event.is_action_pressed("ui_focus_prev") :
				current = current - 1
				if current < 0 :
					current = player_list.size() - 1
			
			MUSE.play_sfx(MUSE.PLAYER_SELECT, team["stream"])
			
			team["players"][player_selected] = current
			update_character_selection(team)
		
		if event.is_action_pressed("ui_start") :
			MUSE.play_sfx(MUSE.READY, team["stream"])
			set_team_ready(team, true)
			if teams[0]["ready"] && teams[1]["ready"] :
				start_ready_timer()
	else :
		if event.is_action_pressed("ui_cancel") && team["ready"] :
			MUSE.play_sfx(MUSE.CANCEL, team["stream"])
			set_team_ready(team, false)
			if not ready_timer.is_stopped() :
				stop_ready_timer()

func init_team(var team) :
	for i in range(team["players"].size()) :
		var player = randi() % player_list.size()
		team["players"][i] = player
		var player_frames = player_list[player].get_node("AnimatedSprite").frames
		get_node(team["node"] + "/Player" + String(i) + "/wait").frames = player_frames
	
	team["stream"] = get_node(team["node"] + "/AudioStreamPlayer2D")
	
	update_character_selection(team)

func update_player_selected(var team, var new_player) :
	
	# hide selection of current player
	get_node(team["node"] + "/Player" + String(team["current"]) + "/select").visible = false
	
	# Update current player
	team["current"] = new_player
	get_node(team["node"] + "/Player" + String(team["current"]) + "/select").visible = true
	
	update_character_selection(team)
	

func update_character_selection(var team) :
	
	var player_selected = team["current"]
	var player = player_list[team["players"][player_selected]]
	
	# Update name
	get_node(team["node"] + "/CharacterSelectionMenu/CharacterSelector/PanelContainer/CharacterName").text = player.get_name().to_upper()
	
	# Update Characteristics
	get_node(team["node"] + "/CharacterSelectionMenu/CharacterInfo/CharacterStats/Offense").text = "Offense : " + String(player.offense_skill) + " / 20"
	get_node(team["node"] + "/CharacterSelectionMenu/CharacterInfo/CharacterStats/Defense").text = "Defense : " + String(player.defense_skill) + " / 20"
	get_node(team["node"] + "/CharacterSelectionMenu/CharacterInfo/CharacterStats/Reflex").text = "Reflexes : " + String(player.reflex_skill) + " / 20"
	get_node(team["node"] + "/CharacterSelectionMenu/CharacterInfo/CharacterStats/MoveSet").text = "Move Set : " + String(player.move_skill) + " / 20"
	get_node(team["node"] + "/CharacterSelectionMenu/CharacterInfo/CharacterStats/TeamSpirit").text = "Team Spirit : " + String(player.teammate_skill) + " / 20"
	
	# Update Animations
	var player_frames = player.get_node("AnimatedSprite").frames
	get_node(team["node"] + "/CharacterSelectionMenu/CharacterAnimations/Waiting").frames = player_frames
	get_node(team["node"] + "/CharacterSelectionMenu/CharacterAnimations/Running").frames = player_frames
	get_node(team["node"] + "/CharacterSelectionMenu/CharacterAnimations/SwordAttack").frames = player_frames
	get_node(team["node"] + "/CharacterSelectionMenu/CharacterAnimations/SwordAttack").frames.set_animation_loop("attack_sword_down", true)
	get_node(team["node"] + "/CharacterSelectionMenu/CharacterAnimations/BowAttack").frames = player_frames
	get_node(team["node"] + "/CharacterSelectionMenu/CharacterAnimations/BowAttack").frames.set_animation_loop("attack_bow_down", true)
	
	# Update player selected
	var player_selected_sprite = get_node(team["node"] + "/Player" + String(player_selected) + "/wait")
	if player.get_name() == "Jerem" :
		player_selected_sprite.position = Vector2(0,-16)
	else :
		player_selected_sprite.position = Vector2(0,0)
	player_selected_sprite.frames = player_frames
	
	# Update big picture
	get_node(team["node"] + "/CharacterSelectionMenu/CharacterInfo/CharacterBigPicture").texture = player.get_node("BigPicture").texture

func set_team_ready(var team, var ready) :
	team["ready"] = ready
	# Update visual
	get_node(team["node"] + "/NotReady").visible = !ready
	get_node(team["node"] + "/Ready").visible = ready

func start_ready_timer() :
	# Show chrono
	var chrono = get_node("Chrono")
	chrono.text = String(int(ready_timer.wait_time))
	chrono.visible = true
	
	# Start timer
	ready_timer.start()
	set_process(true)

func stop_ready_timer():
	ready_timer.stop()
	set_process(false)
	get_node("Chrono").visible = false

func _process(delta):
	# Get chrono values 
	var chrono_value = int(get_node("Chrono").text)
	var time_left = int(round(ready_timer.time_left))
	
	# compare and update
	if chrono_value != time_left :
		get_node("Chrono").text = String(time_left)

func on_ready_timer_timeout():
	set_process(false) # just in case
	
	# Setup teams to emit
	var teams_ready = []
	for i in range(teams.size()) :
		var team = []
		for j in range(teams[i]["players"].size()) :
			team.append(player_list[teams[i]["players"][j]].filename)
		teams_ready.append(team)
	
	# Emit signal
	emit_signal("ready_signal", teams_ready)