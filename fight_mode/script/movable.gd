extends Node

#signal change_object_position_signal
signal output_signal
signal finish_animation_move_signal
var last_path = []
onready var map = get_node("../..")

export (Vector2) var position_index = Vector2(0,0)

var stream_player = AudioStreamPlayer2D.new()

func _ready():
	self.map = get_parent().get_parent()
	
	self.stream_player.bus = "SFX"
	add_child(self.stream_player)
	
#func change_position(var position_index_to):
	# Ask to the map to change the position of the current obj
#	emit_signal("change_object_position_signal", self.position_index, position_index_to, "Characters")
#	update_position_from_index(position_index_to)

func set_position_index(var position_index_to):
	# Output message
	var s = self.get_name() + " makes a move from " + String(self.position_index) + " to " + String(position_index_to)
	emit_signal("output_signal", s)
	self.position_index = position_index_to
	
func update_graphics_position_from_index(var position_index):
	set_position(self.map.get_cell_center_position(position_index))
	
func is_in_range(var position_index_from, var function_range):
	if function_range.f.call_func(self.position_index, position_index_from, function_range.args):
		return true
	return false
	
func move(var path):
	pass
#	#move step by step and check opportunity attack
#	var size = path.size()
#	var offset = 0
#	var cells = []
#	for i in range(size):
#		var k = size - 1 - i
#		#Except the destination cell, check if there is an oppotunity attack on the path
#		if k > 0:
#			var direction = (path[k-1] - path[k]).normalized()
#			cells = map.get_beside_cells(path[k], direction)
#			# if it's the origin cell in the path, check all around
#			if k == size - 1:
#				cells.append(map.get_behind_cell(path[k], -direction))
#		var index_cell = get_opportunity_attack(cells)
#		#if opportunity attack, stop the move of the player
#		if index_cell >= 0:
#			offset = size - i
#			break
#	for i in range(offset):
#		path.pop_front()
#	path.pop_back()
#	if path.size():
#		var position_from = self.position_index
#		var position_to = path.front()
#		animation_move_to_by_path(path)
#		change_position(position_to)

func animation_move_to_by_path(var path):
	var size = path.size()
	for i in range(size):
		var k = size - i - 1
		var direction
		if k != size - 1:
			direction = path[k] - path[k+1]
		else:
			direction = path[k] - self.position_index
		
		animation_move_to(self.map.get_cell_center_position(path[k]), direction)
		yield($Tween, "tween_completed")
	
	emit_signal("finish_animation_move_signal")

# Animation for moving object on the map
# position_from : starting position (not index position)
# position_to : enndig position (not index position)
# time : time of the animation in seconds
func animation_move_to(var position_from, var position_to, var time = 0.5):
	var tween = $Tween
	tween.interpolate_method(self, "set_position", position_from, position_to, time, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()