extends Node

var matrix2d
var size_mat = Vector2(0,0)

func _init():
	init(10,8)
	
func init(var w, var h):
	self.matrix2d = []
	self.matrix2d.resize(w)
	for i in range(w):
		self.matrix2d[i] = []
		self.matrix2d[i].resize(h)
		for j in range(h):
			self.matrix2d[i][j] = []

	self.size_mat = Vector2(w, h)

# TODO : fonction pas complète car si plusieurs objets du même groupe?
# Move the object in group from cell at position_index to cell at destination_index
# position_index : Vector2 Index of the origin position 
# destination_index : Vector2 Index of the destination position
# group : String Name of the group
func move(var position_index, var destination_index, var name):
	var obj = pop_by_name(position_index, name)
	if obj:
		obj.set_position_index(destination_index)
		push(destination_index, obj)
#		print_state_map()
		return true
	return false
	
func add(var obj, var destination_index):
	obj.set_position_index(destination_index)
	push(destination_index, obj)
	
func remove(var obj):
	pop_by_name(obj.position_index, obj.name)
	obj.set_position_index(Vector2(-1,-1))
	
# TODO : fonction pas complète car si plusieurs objets du même groupe?
# Pop an object in group from the cell at position_index
# position_index : Vector2 Index of the origin position 
# group : String Name of the group
func pop_by_group(var position_index, var group):
	for i in range(self.matrix2d[position_index.x][position_index.y].size()):
		if self.matrix2d[position_index.x][position_index.y][i].is_in_group(group):
			var obj = self.matrix2d[position_index.x][position_index.y][i]
			self.matrix2d[position_index.x][position_index.y].remove(i)
			return obj
	return null
	
# TODO : fonction pas complète car si plusieurs objets du même groupe?
# Pop an object in group from the cell at position_index
# position_index : Vector2 Index of the origin position 
# group : String Name of the group
func pop_by_name(var position_index, var name):
	for i in range(self.matrix2d[position_index.x][position_index.y].size()):
		if self.matrix2d[position_index.x][position_index.y][i].name == name:
			var obj = self.matrix2d[position_index.x][position_index.y][i]
			self.matrix2d[position_index.x][position_index.y].remove(i)
			return obj
	return null


# Check if one object of the given group is in the cell at position_index
# position_index : Vector2 Index of the origin position 
# group_name : String Name of the group to search
func is_object_in_cell_by_group(var position_index, var group_name):
	for obj in self.matrix2d[int(round(position_index.x))][int(round(position_index.y))]:
		if obj.is_in_group(group_name):
			return true
	return false

# Check if one object with the given name is in the cell at position_index
# position_index : Vector2 Index of the origin position 
# obj_name : String Name of the object to search
func is_object_in_cell_by_name(var position_index, var obj_name):
	for obj in self.matrix2d[position_index.x][position_index.y]:
		if obj.name == obj_name:
			return true
	return false

# Get cells in a given zona from a cell at position_index
# position_index : Vector2 Index of the origin position of the cell
# distance_func : Function Zone function
# args : Array Argument of the zone function
# can_be_empty : Boolean If true, the cells could be empty
# group_excluded : Array Groups of objects which are not in the searching cells
# group_included : Array Groups of objects which are in the searching cells
func get_cells(var position_index, var distance_func, var args, var can_be_empty = true, var groups_excluded = [], var groups_included = []):
	var add_cell_bool = true
	var cells = []
	for i in range(size_mat.x):
		for j in range(size_mat.y):
			if distance_func.call_func(position_index, Vector2(i, j), args):
				if !groups_excluded.empty():
					for k in groups_excluded:
						if is_object_in_cell_by_group(Vector2(i,j), k):
							add_cell_bool = false
				if !groups_included.empty():
					var b = false
					for k in groups_included:
						if is_object_in_cell_by_group(Vector2(i,j), k):
							b = true
					if !b:
#						print("group included : ", Vector2(i,j))
						add_cell_bool = false
				if is_empty_cell(Vector2(i,j)) == true && can_be_empty:
					add_cell_bool = true
				if add_cell_bool:
					cells.append(Vector2(i,j))
			add_cell_bool = true
	return cells

# Check if the cell is empty
# position_index : Vector2 Index of the origin position of the cell
func is_empty_cell(var position_index):
	return self.matrix2d[position_index.x][position_index.y].empty()

# Check if the cell is valid according to the input parameters
# position_index : Vector2 Index of the origin position of the cell
# can_be_empty : Boolean If true, the cells could be empty
# group_included : Array Groups of objects which are in the searching cells
# group_excluded : Array Groups of objects which are not in the searching cells
func is_cell_valid(var position_index, var can_be_empty, var included_groups, var excluded_groups):
	if !excluded_groups.empty():
		for k in excluded_groups:
			if is_object_in_cell_by_group(position_index, k):
				return false
	if !included_groups.empty():
		var b = false
		for k in included_groups:
			if is_object_in_cell_by_group(position_index, k):
				b = true
		if !b:
			return false
	if is_empty_cell(position_index) == true && !can_be_empty:
		return false
	return true

# TODO : attention ne retourn qu'un obj
# Get one obj in the given group in the cell at position_index
func get_obj_by_group(var position_index, var group_name):
	for obj in self.matrix2d[position_index.x][position_index.y]:
		if obj.is_in_group(group_name):
			return obj
	return null

# Check if the cell at position_index1 is the neighbour of the cell at position_index2
# position_index1 : Vector2 Index of the position of the cell number 1
# position_index2 : Vector2 Index of the position of the cell number 2
func is_next_to(var position_index1, var position_index2):
	var vec = position_index2 - position_index1
	if vec.length() == 1:
		return true
	return false
	
func get_next_to(var position_index):
	return [Vector2(position_index.x+1, position_index.y), Vector2(position_index.x-1, position_index.y), Vector2(position_index.x, position_index.y+1), Vector2(position_index.x, position_index.y-1)]
	
func get_beside_cells(var position_index, var direction):
	var t = get_next_to(position_index)
	var t2 = []
	for cell_index in t:
		if (cell_index - position_index.normalized()).dot(direction) == 0:
			t2.append(cell_index)
	return t2
	
func get_behind_cell(var position_index, var direction):
	return position_index - direction
	
# Get the closest cell which contains an object in the given group in the given direction and angle
# vec_ref : Vector2 Reference vector to compute the angle
# position_index : Vector2 Index of the position of the cell
# angle : Float angle of the view from the cell position index
# offset_angle : Float Offset of the opening angle
# included_group : String Group of objects which is in the searching cells
func get_closest_cell_by_angle(var vec_ref, var position_index, var angle, var offset_angle, var included_group):
	var closest_cell = null
	var closest_dist = -1
	for i in range(size_mat.x):
		for j in range(size_mat.y):
			var pos_ind = Vector2(i,j)
			var angle_new = vec_ref.angle_to((pos_ind - position_index).normalized())
			if angle_new > angle - offset_angle && angle_new < angle + offset_angle:
				if get_obj_by_group(pos_ind, included_group):
					var d = Tools.manhattan_dist(position_index, pos_ind)
					if closest_dist == -1 || d < closest_dist:
						closest_dist = d
						closest_cell = pos_ind
	return closest_cell
	
# Get the closest cell which contains an object in the given group
# position_index : Vector2 Index of the position of the cell
# included_group : String Group of objects which is in the searching cells
func get_closest_cell(var position_index, var included_group):
	var closest_cell = null
	var closest_dist = -1
	for i in range(size_mat.x):
		for j in range(size_mat.y):
			var pos_ind = Vector2(i,j)
			if get_obj_by_group(pos_ind, included_group):
				var d = Tools.manhattan_dist(position_index, pos_ind)
				if closest_dist == -1 || d < closest_dist:
					closest_dist = d
					closest_cell = pos_ind
	return closest_cell

# Get the closest cell in the array of cells
# position_index : Vector2 Index of the position of the cell
# cells : array Array of cells
func get_closest_cell_in_cells(var position_index, var cells):
	if cells && cells.size():
		var closest_cell = cells[0]
		var dist = Tools.manhattan_dist(position_index, closest_cell)
		for cell in cells:
			var d = Tools.manhattan_dist(position_index, cell)
			if d < dist:
				d = dist
				closest_cell = cell
		return closest_cell
	return null
			
		
# Process a path with A* algorithm to obtain the shortest one
# position_index_from : Vector2 Index of the origin position of the cell
# position_index_to : Vector2 Index of the destination position of the cell
func process_path(var position_index_from, var position_index_to, var black_list_groups = [], var white_list_groups = []):
	return Tools.shortest_path(position_index_from, position_index_to, self, black_list_groups, white_list_groups)
	
func get_paths_around_cell(var position_index_from, var position_index_to, var black_list_groups = [], var white_list_groups = []):
	var cells = get_next_to(position_index_to)
	var paths = []
	for cell in cells:
		if cell == position_index_from:
			paths.append([cell])
		else:
			paths.append(process_path(position_index_from, cell, black_list_groups, white_list_groups))
	return paths

func get_shortest_path_around_cell(var position_index_from, var position_index_to, var included_groups = [], var excluded_groups = []):
	var paths = get_paths_around_cell(position_index_from, position_index_to, excluded_groups, included_groups)
	var current_path = null
	var size = null
	for path in paths:
		if path:
			if current_path == null || path.size() < size:
				current_path = path
				size = current_path.size()
	return current_path

func print_state_map():
	print("-------------------------------------------")
	print("State of the matrix :")
	for i in range(size_mat.x):
		for j in range(size_mat.y):
			if !self.matrix2d[i][j].empty():
				print("objects in : (" , i ,",",j,")")			
				for k in self.matrix2d[i][j]:
					print("-> object : " , k.name)
					
					
# PRIVATE :
	
# Push an object to the cell at position_index
# position_index : Vector2 Index of the origin position 
# obj : Object Object to push in the cell
func push(var position_index, var obj):
	self.matrix2d[position_index.x][position_index.y].append(obj)