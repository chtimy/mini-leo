extends "res://fight_mode/script/character.gd"

func play():
	var s = self.get_name() + " turn"
	emit_signal("output_signal", s)
	if !try_get_weapon():
		try_attack_enemi()
	end_turn()
	
func try_get_weapon():
	if self.weapon:
		return false
	var cell = self.map.get_closest_cell(self.position_index, "Weapons")
	if !cell:
		return false
	var path = self.map.process_path(self.position_index, cell, ["Players"])
	var path_out = []
	if path && path.size():
		var size = path.size()
		var dist = self.displacement_function.args[0]
		var s = min(size, dist)
		for i in range(s):
			var p = path.pop_back()
			path_out.push_front(p)
		
	var weapon = self.map.get_obj_by_group(cell, "Weapons")
	if !weapon:
		return false
	if path_out:
		move(path_out)
	self.map.pop(cell, "Weapons")
	self.map.remove_child(weapon)
	grab_weapon(weapon)
	return true
	
func try_attack_enemi():
	if !self.weapon:
		return false
	var cell = self.map.get_closest_cell(self.position_index, "Players")
	if !cell:
		return false
#	print("cell attack : ", cell)
	map.print_state_map()
	var path = self.map.get_shortest_path_around_cell(self.position_index, cell, ["Players"])
#	print("path attack : ", path)
	var path_out = []
	if path && path.size():
		var size = path.size()
		var dist = self.displacement_function.args[0]
#		print("distance_attack:", dist)
		# dist+1 -> the first cell of the path is the current position of the character
		var s = min(size, dist+1)
		for i in range(s):
			var p = path.pop_back()
			path_out.push_front(p)
		
	var enemi = self.map.get_obj_by_group(cell, "Players")
	if !enemi:
		return false
	if path_out.size() > 1:
		move(path_out)
		yield(self, "finish_animation_move_signal")
	attack(enemi)
	return true
	
func _ready():
	self.displacement_function = {"f" : funcref(Tools, "circle_area_func"), "args" : [4]}
	
func end_turn():
	emit_signal("end_turn_signal")