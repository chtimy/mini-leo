extends "res://fight_mode/script/character.gd"

signal mark_passing_player_signal

enum MODE_PLAYER{MOVE_CONTROL, PASS_CONTROL}

#debug
var DRAW_DIRECTION_PASS = false

#States
var CONTROL_MODE = MOVE_CONTROL
var DEAD_ZONE_JOYSTICK = 0.2

onready var turn_handler = get_node("../../..")
var old_joystick_move
var last_input = null
var flip_joystick_dead_zone = false
var input_joystick_direction_timers = []

func _ready():
	$AnimatedSprite.set_material($AnimatedSprite.get_material().duplicate())
	displacement_function = {"f" : funcref(Tools, "circle_area_func"), "args" : [self.nb_move]}
	for i in 4:
		var timer = Timer.new()
		timer.one_shot = true
		timer.wait_time = 0.2
		add_child(timer)
		self.input_joystick_direction_timers.append(timer)
	set_enable(false)

func _draw():
	if DRAW_DIRECTION_PASS:
		var device = 0
		if Input.is_joy_known(1) : # if there is a second controller
			device = team
		var joystick_move = Vector2(Input.get_joy_axis(device, JOY_ANALOG_LX), Input.get_joy_axis(device, JOY_ANALOG_LY))
		var dead_zone = 0.5
		if joystick_move.length() >= 0.5:
			$CursorPass.set_visible(true)
			var nb_tirets = self.distance_pass / (Global.SIZE_TIRETS_PASS.width*2.0)
			var rot = Vector2(0,1).angle_to(joystick_move.normalized())
			var vec_rot = Vector2(0,1).rotated(rot)
			var offset = vec_rot * Global.SIZE_TIRETS_PASS.offset
			var offset2 = offset + vec_rot * $CursorPass.texture.get_height() * $CursorPass.scale.y / 2.0
			for i in nb_tirets:
				if i%2 == 1:
					draw_line(joystick_move.normalized() * Global.SIZE_TIRETS_PASS.width * (i-1) + offset2, joystick_move.normalized() * Global.SIZE_TIRETS_PASS.width * i + offset2, Color(1,0,0), Global.SIZE_TIRETS_PASS.width)
			$CursorPass.set_position(Vector2(0,0))
			$CursorPass.set_rotation(rot)
			$CursorPass.set_position(vec_rot * Global.SIZE_TIRETS_PASS.offset)
		else:
			$CursorPass.set_visible(false)
	else:
		$CursorPass.set_visible(false)
			
		
func _input(event):
	# check device if there is two devices
	if !Input.is_joy_known(1) || event.device == team :
		var joystick_move = Vector2(Input.get_joy_axis(event.device, JOY_ANALOG_LX), Input.get_joy_axis(event.device, JOY_ANALOG_LY))
		if event is InputEventKey:
			if event.scancode == KEY_D: # For debug
				map.print_state_map()
		if CONTROL_MODE == MOVE_CONTROL:
			if event.is_action_pressed("ui_cancel"):
				on_bonus_timer_timeout()
				for charac in self.turn_handler.characters:
					if charac.team != self.team:
						if charac.get_current_animation() == "afraid":
							var anim = charac.get_animation_name(charac.last_direction, "wait")
							charac.set_animation(anim)
				end_turn()
			elif event.is_action_pressed("ui_mode_pass") && can_pass():
				# GRAPHICS
				self.map.set_move_zone_visibility(false)
				self.map.move_cursor_to(self.position_index)
				self.map.set_cursor_vibility(false)
				self.map.set_draw_arrow(false)
				self.map.hide_indicator("move_indicator")
				self.map.hide_indicator("grab_indicator")
				self.map.hide_indicator("steal_indicator")
				self.map.hide_indicator("attack_sword_indicator")
				self.map.hide_indicator("attack_bow_indicator")
				self.map.hide_indicator("pass_indicator")
				
				set_target_enemis_visibility(false)
				
				
				CONTROL_MODE = PASS_CONTROL
			elif event.is_action_pressed("ui_right"):
				direction_move_input(Global.RIGHT)
			elif event.is_action_pressed("ui_left"):
				direction_move_input(Global.LEFT)
			elif event.is_action_pressed("ui_up"):
				direction_move_input(Global.UP)
			elif event.is_action_pressed("ui_down"):
				direction_move_input(Global.DOWN)
			elif event.is_action_pressed("ui_accept"):
				var button_accept_move_input = button_accept_move_input()
				if button_accept_move_input:
					yield(button_accept_move_input, "completed")
				if states.check_end_turn():
					end_turn()
			elif event.is_action_pressed("ui_select"):
				var button_action_move_input = button_action_move_input()
				var end_turn
				if typeof(button_action_move_input) != TYPE_BOOL:
					yield(button_action_move_input, "completed")
				if states.check_end_turn():
					end_turn()
	#		if joystick_move.length() >= dead_zone:
	#			joystick_for_move(Tools.direction_input_to_cste(InputEventJoypadMotion.axis))
		elif CONTROL_MODE == PASS_CONTROL:
			if event.is_action_pressed("ui_cancel"):
				on_bonus_timer_timeout()
				self.map.hide_indicator("pass_indicator")
				end_turn()
			# If the player release the button pass of the controller
			elif event.is_action_released("ui_mode_pass"):
				# GRAPHICS
				set_draw_direction_pass(false)
				if states.can_move():
					self.map.process_move_zone(self)
					self.map.set_move_zone_visibility(true)
				self.map.move_cursor_to(self.position_index)
				self.map.set_cursor_vibility(true)
				if states.can_attack() && self.weapon:
					target_enemis(self.position_index, self.weapon.function_range)
					set_target_enemis_visibility(true)
				if can_pass():
					self.map.move_indicator("pass_indicator", self.position_index)
					self.map.show_indicator("pass_indicator")
					
				# ANIMATION
				var animation = get_animation_name(last_direction, "wait")
				set_animation(animation)
				
				CONTROL_MODE = MOVE_CONTROL
				self.old_joystick_move = null
			elif event.is_action_pressed("ui_accept"):
				var button_accept_pass_input = button_accept_pass_input()
				if button_accept_pass_input:
					yield(button_accept_pass_input, "completed")
			# if the joystick moves
			elif joystick_move.length() >= DEAD_ZONE_JOYSTICK:
				set_draw_direction_pass(true)
				self.flip_joystick_dead_zone = true
				#condition en plus pour éviter trop de calculs : verifier que le vecteur ne sort pas du champs angulaire déjà vérifié
				#Si le joueur a déjà commencé à jouer ou si il vise déjà mais qu'il vise ailleurs (seuil)
				#Attention au sens des conditions
				if !self.old_joystick_move || abs(self.old_joystick_move.angle_to(joystick_move)) > deg2rad(OFFSET_ANGLE_IN_DEGREES_FOR_PASS/4):
					direction_pass_input(joystick_move.angle())
					self.old_joystick_move = joystick_move
			# joystick in the rest position
			elif self.flip_joystick_dead_zone:
				self.flip_joystick_dead_zone = false
				self.old_joystick_move = null
				clear_teammate()
				
				# GRAPHICS
				set_draw_direction_pass(false)
				
				# ANIMATION
				var animation = get_animation_name(last_direction, "wait")
				set_animation(animation)
			

func play(var status_start = states.NORMAL_STATUS):
	states.start(status_start)
	#transition state from NO_MODE to NORMAL_STATUS
	if Input.is_action_pressed("ui_mode_pass") && can_pass():
		self.map.set_move_zone_visibility(false)
		self.map.move_cursor_to(self.position_index)
		self.map.set_cursor_vibility(false)
		self.map.set_draw_arrow(false)
		self.map.hide_indicator("move_indicator")
		self.map.hide_indicator("grab_indicator")
		self.map.hide_indicator("steal_indicator")
		self.map.hide_indicator("pass_indicator")
		set_target_enemis_visibility(false)
		CONTROL_MODE = PASS_CONTROL
	#transition state from NO_MODE to NORMAL_STATUS
	else:
		if states.can_move():
			self.map.process_move_zone(self)
			self.map.set_move_zone_visibility(true)
		self.map.move_cursor_to(self.position_index)
		self.map.set_cursor_vibility(true)
		if states.can_attack() && self.weapon:
			target_enemis(self.position_index, self.weapon.function_range)
			set_target_enemis_visibility(true)
		if can_pass():
			self.map.move_indicator("pass_indicator", self.position_index)
			self.map.show_indicator("pass_indicator")
		if can_grab(self.position_index):
			self.map.move_indicator("grab_indicator", self.position_index)
			self.map.show_indicator("grab_indicator")
		
			
		CONTROL_MODE = MOVE_CONTROL
		
	self.map.focus_character_arrow(self, Vector2(0,-60))
	
	set_enable(true)
	
func end_turn(var charac = null, var state = null):
	set_enable(false)
	clear()
	.end_turn(charac, state)
	

func button_accept_pass_input():
	if self.teammate_for_pass:
		set_draw_direction_pass(false)
		set_enable(false)
		var pass_object_action = pass_object_action()
		if pass_object_action is GDScriptFunctionState:
			yield(pass_object_action, "completed")
#		set_enable(true)

# When the player check the pass direction (move the left joystick during the LT is pushed on the XBOX controller), thins function is called
func direction_pass_input(var angle):
	clear_teammate()
	var offset_angle_in_radians = deg2rad(OFFSET_ANGLE_IN_DEGREES_FOR_PASS)
	var characters = self.turn_handler.characters
	
	# ANIMATION
	var direction = Tools.get_direction_cste_from_vec(Vector2(1.0,0.0).rotated(angle))
	var animation = get_animation_name(direction, "pass")
	set_animation(animation)
	
	# look for the teammate in the pass direction
	var teammate = null
	var distance_min = -1
	for character in characters:
		if character.team == self.team:
			# angle between the ref vec and the vector from the current player and the player to test
			var angle_new = Vector2(1, 0).angle_to((map.get_cell_center_position(character.position_index) - map.get_cell_center_position(self.position_index)).normalized())
			var distance = character.position_index.distance_to(self.position_index)
			# if it's in the opened angle and closest one
			if character != self && angle_new > angle - offset_angle_in_radians && angle_new < angle + offset_angle_in_radians && (distance_min == -1 || distance_min >= distance):
				distance_min = distance
				teammate = character
	# if there is teammate
	if teammate:
		# ANIMATION
		for charac in self.turn_handler.characters:
			if charac.team == self.team && charac != self && charac != teammate:
				var dir = charac.last_direction
				var animation_teammate = charac.get_animation_name(dir, "wait")
				charac.set_animation(animation_teammate)
		var dir = (self.position_index - teammate.position_index).normalized()
		var animation_teammate = teammate.get_animation_name(Tools.get_direction_cste_from_vec(dir), "get_pass", false)
		teammate.set_animation(animation_teammate)
		
		self.teammate_for_pass = teammate

# When the player wants to move the map cursor (left jostick moves on the XBOX controller)
func direction_move_input(var direction):
	if self.input_joystick_direction_timers[direction].is_stopped():
		self.map.move_cursor(direction)
		# Check if can move
		if can_move(map.get_cursor_position_index()):
			self.last_path = map.get_existing_path(map.get_cursor_position_index())
			self.map.set_draw_arrow(true, self.last_path)
			self.map.show_indicator("move_indicator")
			self.map.move_indicator("move_indicator", self.map.get_cursor_position_index())
		else:
			self.map.set_draw_arrow(false)
			self.map.hide_indicator("move_indicator")
			
		if can_attack(map.get_cursor_position_index()):
			var enemi = self.map.get_obj_by_group(self.map.get_cursor_position_index(), "Characters")
			if enemi:
				enemi.set_animation("afraid")
				if weapon.is_in_group("Swords"):
					self.map.show_indicator("attack_sword_indicator")
					self.map.move_indicator("attack_sword_indicator", self.map.get_cursor_position_index())
				if weapon.is_in_group("Bows"):
					self.map.show_indicator("attack_bow_indicator")
					self.map.move_indicator("attack_bow_indicator", self.map.get_cursor_position_index())
					
		else:
			for charac in self.turn_handler.characters:
				if charac.team != self.team:
					if charac.get_current_animation() == "afraid":
						var anim = charac.get_animation_name(charac.last_direction, "wait")
						charac.set_animation(anim)
			self.map.hide_indicator("attack_bow_indicator")
			self.map.hide_indicator("attack_sword_indicator")
			
		if can_grab(map.get_cursor_position_index()):
			self.map.show_indicator("grab_indicator")
			self.map.move_indicator("grab_indicator", self.map.get_cursor_position_index())
		else:
			self.map.hide_indicator("grab_indicator")
			
		var charac = map.get_obj_by_group(map.get_cursor_position_index(), "Characters")
		if charac && can_steal(charac):
			self.map.show_indicator("steal_indicator")
			self.map.move_indicator("steal_indicator", self.map.get_cursor_position_index())
		else:
			self.map.hide_indicator("steal_indicator")
			
		self.input_joystick_direction_timers[direction].start()

# When the player confirm for moving (push A on the XBOX controller), this function is called
func button_accept_move_input():
	# if player is in normal mode turn or he made a pass and if the move action is available
	if can_move(map.get_cursor_position_index()):
		MUSE.play_sfx(MUSE.OPTION_SELECT, null, false)
		
		# GRAPHICS
		self.map.set_move_zone_visibility(false)
		self.map.set_draw_arrow(false)
		self.map.hide_indicator("move_indicator")
		self.map.hide_indicator("grab_indicator")
		self.map.hide_indicator("steal_indicator")
		self.map.hide_indicator("pass_indicator")
		self.map.set_cursor_vibility(false)
		set_target_enemis_visibility(false)

		set_enable(false)
		var move_action = move(self.last_path)
		if !move_action is VisualScriptFunction:
			yield(move_action, "completed")
		set_enable(true)

		# GRAPHICS
		self.map.move_cursor_to(self.position_index)
		self.map.set_cursor_vibility(true)
		if weapon && states.can_attack():
			target_enemis(self.position_index, self.weapon.function_range)
			set_target_enemis_visibility(true)
		if can_pass():
			self.map.move_indicator("pass_indicator", self.position_index)
			self.map.show_indicator("pass_indicator")
		if can_grab(map.get_cursor_position_index()):
			self.map.show_indicator("grab_indicator")
			self.map.move_indicator("grab_indicator", self.map.get_cursor_position_index())

# The player try attack an enemi (X pushed on the xbox controller), this function is called
# also called when try to steal an enemi
func button_action_move_input():
	var position_index_to = map.get_cursor_position_index()
	var action_done = false
	var enemi = self.map.get_obj_by_group(position_index_to, "Characters")
	if enemi && enemi.position_index != self.position_index:
		#if player tries to attack an enemi
		if can_attack(position_index_to):
			if self.enemis_to_attack.find(enemi) != -1:
				#MUSE.play_sfx(MUSE.OPTION_SELECT, null, false)
				
				#GRAPHICS
				set_target_enemis_visibility(false)
				self.map.set_move_zone_visibility(false)
				self.map.set_cursor_vibility(false)
				self.map.hide_all_indicators()
				
				
				set_enable(false)
				var attack = attack(enemi)
				if attack is GDScriptFunctionState:
					yield(attack, "completed")
				set_enable(true)
				action_done = true
		elif can_steal(enemi) :
			#MUSE.play_sfx(MUSE.OPTION_SELECT, null, false)
			set_enable(false)
			var steal = steal(enemi)
			if steal != null :
				action_done = yield(steal, "completed")
			else:
				action_done = steal
			self.map.hide_indicator("steal_indicator")
			set_enable(true)
	elif can_grab(position_index_to):
		set_enable(false)
		var grab_weapon = grab_weapon_on_the_floor()
		if grab_weapon != null:
			yield(grab_weapon, "completed")
		action_done = true
		self.map.hide_indicator("grab_indicator")
		set_enable(true)
	
	# GRAPHICS
	self.map.process_move_zone(self)
	if action_done && states.can_move() && self.map.nb_existing_path():
		self.map.set_move_zone_visibility(true)
		self.map.move_cursor_to(self.position_index)
		self.map.set_cursor_vibility(true)
	if can_pass():
		self.map.move_indicator("pass_indicator", self.position_index)
		self.map.show_indicator("pass_indicator")
	if weapon && states.can_attack():
		target_enemis(self.position_index, self.weapon.function_range)
		set_target_enemis_visibility(true)
		if can_attack(map.get_cursor_position_index()):
			var charac = self.map.get_obj_by_group(self.map.get_cursor_position_index(), "Characters")
			if charac:
				charac.set_animation("afraid")
				if weapon.is_in_group("Swords"):
					self.map.show_indicator("attack_sword_indicator")
					self.map.move_indicator("attack_sword_indicator", self.map.get_cursor_position_index())
				else:
					self.map.hide_indicator("attack_sword_indicator")
				if weapon.is_in_group("Bows"):
					self.map.show_indicator("attack_bow_indicator")
					self.map.move_indicator("attack_bow_indicator", self.map.get_cursor_position_index())
				else:
					self.map.hide_indicator("attack_bow_indicator")
		
	
	return action_done

# Interception with QTE
func interception(var position_index, var object):
#	set_process_input(false)
	# run qte
	var qte = self.get_node("QTE").launch_random_qte(team, self.reflex_skill, 1)
	
	# 0 : no input, 1 : wrong input or wrong timing, 2 : good input in time
	var ret = 0
	if qte :
		#MUSE.play_sfx(MUSE.DRUMS, self.stream_player)
		# Wait for completed signal
		ret = yield(qte, "completed")
		#MUSE.stop_sfx(self.stream_player)
	
	# If action pressed, interception try
	if ret > 0 :
		var sup = .interception(position_index,object)
		if sup != null && typeof(sup) != TYPE_BOOL :
			yield(sup, "completed")
	
	# Return true only if fhe right button is pressed in time
	return (ret == 2)

func receive_weapon(var weapon, var bonus, var nb):
	# run qte
	var qte = self.get_node("QTE").launch_random_qte(team, self.reflex_skill+10)
	
	# 0 : no input, 1 : wrong input or wrong timing, 2 : good input in time
	var ret = 0
	if qte :
		#MUSE.play_sfx(MUSE.DRUMS, self.stream_player)
		# Wait for completed signal
		ret = yield(qte, "completed")
		#MUSE.stop_sfx(self.stream_player)
	
	# If action pressed in right time grab weapon
	if ret == 2 :
		return .receive_weapon(weapon, bonus, nb)
	
	# Return true only if fhe right button is pressed in time
	return false

func got_weapon_stealed() :
	# run qte
	var qte = self.get_node("QTE").launch_random_qte(team, self.reflex_skill)
	
	# 0 : no input, 1 : wrong input or wrong timing, 2 : good input in time
	var ret = 0
	if qte :
		#MUSE.play_sfx(MUSE.DRUMS, self.stream_player)
		# Wait for completed signal
		ret = yield(qte, "completed")
		#MUSE.stop_sfx(self.stream_player)
	
	# If action pressed in right time keep weapon
	if ret == 2 :
		#MUSE.play_sfx(MUSE.STEAL_FAILED)
		return null
	
	return .got_weapon_stealed()

func set_target_enemis_visibility(var visible):
	for enemi in self.enemis_to_attack:
		enemi.show_target_symbol(visible)

func target_enemis(var position_index, var function_range):
	self.enemis_to_attack.clear()
	var characters = self.turn_handler.characters
	for character in characters:
		if character.team != self.team && character.is_in_range(position_index, function_range) :
			self.enemis_to_attack.append(character)

func set_enable(var boolean):
	set_process_input(boolean)

func clear():
	self.map.set_move_zone_visibility(false)
	self.map.set_cursor_vibility(false)
	set_target_enemis_visibility(false)
	self.enemis_to_attack.clear()
	clear_teammate()
	self.map.set_draw_arrow(false)
	set_draw_direction_pass(false)
	self.map.hide_all_indicators()
	
	
func set_draw_direction_pass(var boolean):
	self.DRAW_DIRECTION_PASS = boolean
	update()
