shader_type canvas_item;
uniform vec4 u_color : hint_color = vec4(0,0,0,1);
uniform vec3 u_test;

void fragment() {
	vec4 colorTexture = texture(TEXTURE, UV);
	if(colorTexture == vec4(0,0,0,1))
	{
//		colorTexture = vec4(u_test, 1.0);//u_colorBorder;
		colorTexture = u_color;
	}
	COLOR = colorTexture;
//	COLOR = vec4(u_test, 1);
//	COLOR = u_color;
}
