extends Control

var scenes = {
	"title": preload("res://scenes/TitleScene.tscn"),
	"select": preload("res://fight_mode/scenes/TeamSelectionMenu.tscn"),
	"credits": preload("res://scenes/Credits.tscn"),
	"intro": preload("res://scenes/Intro.tscn")
}

var last_teams = null # for rematches


func _ready():
	# Set windows in the middle
	var screen_size = OS.get_screen_size(0)
	var window_size = OS.get_window_size()
	OS.set_window_position(screen_size*0.5 - window_size*0.5)
	
	# To random
	randomize()
	
	# Play music
#	MUSE.play_background_music(MUSE.TITLE)
	
	# Init Devices Controller
	Input.connect("joy_connection_changed", self, "on_joy_connection_changed")
	
	# First check of controlers
	if not Input.is_joy_known(0) or not Input.is_joy_known(1) :
		if not Input.is_joy_known(0) :
			get_node("TwoDevicesController/HBoxContainer/LeftController").modulate = Color(0.5,0.5,0.5)
		if not Input.is_joy_known(1) :
			get_node("TwoDevicesController/HBoxContainer/RightController").modulate = Color(0.5,0.5,0.5)
		
		pause_game()
	
	# Instance Intro Menu
	load_new_scene("intro")
	

func on_joy_connection_changed(var index, var connected) :
	
	var control_node = get_node("TwoDevicesController")
	var controller = null
	if index == 0 :
		controller = control_node.get_node("HBoxContainer/LeftController")
	elif index == 1 :
		controller = control_node.get_node("HBoxContainer/RightController")
	
	if connected :
		controller.modulate = Color(1,1,1)
		if Input.is_joy_known(0) and Input.is_joy_known(1) :
			unpause_game()
	else :
		controller.modulate = Color(0.5,0.5,0.5)
		pause_game()

func pause_game() :
	MUSE.pause_background_music()
	get_tree().paused = true
	get_node("TwoDevicesController").visible = true

func unpause_game() :
	# wait for it
	var t = Timer.new()
	t.set_wait_time(1)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	t.queue_free()
	
	get_node("TwoDevicesController").visible = false
	get_tree().paused = false
	MUSE.unpause_background_music()

func on_option_selected(var option) :
	match option:
		"Play":
			MUSE.play_sfx(MUSE.OPTION_SELECT)
			load_new_scene("select")
		"Credits":
			MUSE.play_sfx(MUSE.OPTION_SELECT)
			load_new_scene("credits")
		"Rematch":
			MUSE.play_sfx(MUSE.OPTION_SELECT)
			on_ready_signal(last_teams)
		"Title":
			load_new_scene("title")
		"Quit":
			MUSE.play_sfx(MUSE.CANCEL)
			get_tree().quit()
# Load battle scene
func on_ready_signal(var teams) :
	# Remember teams selected for potential rematches
	last_teams = teams
	
	var battle_scene = load("res://fight_mode/scenes/Round.tscn")
	
	# Clear current scene
	var current_scene = get_node("CurrentScene")
	for n in current_scene.get_children() :
		current_scene.remove_child(n)
		n.queue_free()
	
	var battle = battle_scene.instance()
	battle.connect("end_game_signal", self, "on_end_game_signal")
	
	current_scene.add_child(battle)
	battle.init_game(teams)

func on_end_game_signal(var teams, var winners) :
	
	var end_scene = load("res://scenes/EndGame.tscn")
	
	# Clear current scene
	var current_scene = get_node("CurrentScene")
	for n in current_scene.get_children() :
		current_scene.remove_child(n)
		n.queue_free()
	
	var end_game = end_scene.instance()
	end_game.connect("option_selected", self, "on_option_selected")
	
	current_scene.add_child(end_game)
	end_game.init(teams, winners)
	
	MUSE.play_background_music(MUSE.TITLE)


func load_new_scene(var scene_name) :
	# Clear current scene
	var current_scene = get_node("CurrentScene")
	for n in current_scene.get_children() :
		current_scene.remove_child(n)
		n.queue_free()
	
	# Load new scene
	var new_scene = scenes[scene_name].instance()
	current_scene.add_child(new_scene)
	
	# Connection
	match scene_name :
		"select" :
			new_scene.connect("ready_signal", self, "on_ready_signal")
		_ :
			new_scene.connect("option_selected", self, "on_option_selected")
		
	
	return new_scene


func _notification(var what):
    if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
        get_tree().quit()